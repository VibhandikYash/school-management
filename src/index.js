import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import store from './redux/store';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import Routes from './routes';
// import Demo from './component/common/demo';

const font = "'Lato', sans-serif";

const theme = createMuiTheme({
    fontFamily: font,
    typography: {
        fontFamily: font,
        useNextVariants: true,
      },
});

function BootStrapApp() {
    return (
        <Provider store={store}>
            <MuiThemeProvider theme={theme}>
                <Routes />
            </MuiThemeProvider>
        </Provider>
    )
}

ReactDOM.render(<BootStrapApp />, document.getElementById('root'));
registerServiceWorker();
