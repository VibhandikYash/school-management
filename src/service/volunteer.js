import { API, graphqlOperation } from 'aws-amplify';
import mutations from '../graphql/mutations';

const VolunteerService = {

    createEvent: async event => {
        const createdEvent = await API.graphql(graphqlOperation(mutations.createEvent, { input: event }));
        return createdEvent;
    }
}

export default VolunteerService;
