import React, { Component, Fragment } from 'react';
import InfiniteScroll from 'react-infinite-scroller';

import FeedCard, { FakeCard } from '../component/FeedCard/FeedCard';
import CreatePostDialog from '../component/CreatePost/PostManageDialog';
import CreatePostPanel from '../component/CreatePost/CreatePostPanel';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchUserCatch } from '../redux/actions/cache';
import {
  fetchPost,
  editPost, deletePost,
  saveComment, updateComment, deleteComment,
  unlikePost, likePost
} from '../redux/actions/feed-post';
import { FeedSelector } from '../redux/selector';
import { handleOpenDialog } from '../redux/actions/post-manage';

class Home extends Component {

  componentDidMount() {
    this.props.fetchUserCatchAction.request();
  }

  loadItems(page) {
    const { fetchPostAction, userGroups } = this.props;
    const groups = userGroups.map(group => group.groupid);
    const query = {
      'page': page - 1,
      'itemsperpage': 10,
      'groups': groups
    }
    fetchPostAction.request(query);
  }

  render() {
    const {
      feeds,
      userGroups,
      currentUser,
      query: { hasMoreItems },
      saveCommentAction,
      updateCommentAction,
      deleteCommentAction,
      editPostAction,
      deletePostAction,
      likePostAction,
      unlikePostAction,
    } = this.props;
    const items = [];
    const fake = Array.from({ length: 2 }, (e, index) => (<FakeCard key={index} />));

    feeds.forEach((feed, i) => {
      items.push(
        <FeedCard
          key={i}
          data={feed}
          editMode
          currentUser={currentUser}
          editPostAction={editPostAction}
          deletePostAction={deletePostAction}
          saveCommentAction={saveCommentAction}
          updateCommentAction={updateCommentAction}
          deleteCommentAction={deleteCommentAction}
          likePostAction={likePostAction}
          unlikePostAction={unlikePostAction}
        />);
    });

    if (userGroups.length !== 0) {
      return (
        <Fragment>
          < CreatePostPanel onClick={this.props.openDialogAction} />
          <CreatePostDialog />
          <InfiniteScroll
            pageStart={0}
            loadMore={this.loadItems.bind(this)}
            hasMore={hasMoreItems}
          >
            {feeds.length > 0 ? items : fake}
          </InfiniteScroll>
        </Fragment>
      )
    }
    return (<div />)
  }
}

const mapStateToProps = state => ({
  currentUser: state.currentUser,
  feeds: FeedSelector.getPostsWithUser(state),
  query: state.feed.query,
  userGroups: state.currentUser.userInfo.Groups ?
    Object.values(state.currentUser.userInfo.Groups) : []
});

const mapDispatchToProps = dispatch => ({
  fetchPostAction: bindActionCreators(fetchPost, dispatch),
  fetchUserCatchAction: bindActionCreators(fetchUserCatch, dispatch),
  editPostAction: bindActionCreators(editPost, dispatch),
  deletePostAction: bindActionCreators(deletePost, dispatch),
  saveCommentAction: bindActionCreators(saveComment, dispatch),
  updateCommentAction: bindActionCreators(updateComment, dispatch),
  deleteCommentAction: bindActionCreators(deleteComment, dispatch),
  likePostAction: bindActionCreators(likePost, dispatch),
  unlikePostAction: bindActionCreators(unlikePost, dispatch),
  openDialogAction: bindActionCreators(handleOpenDialog, dispatch),
})

export default connect(mapStateToProps, mapDispatchToProps)(Home);
