import React, { Component } from 'react';
import HorizontalStepper from '../component/HorizontalStepper';
import { fetchUserCatch } from '../redux/actions/cache';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

export class AddEditVolunteer extends Component {

  componentDidMount() {
    this.props.fetchUserCatchAction.request();
  }

  render() {
    return (
      <div>
        <HorizontalStepper />
      </div>
    )
  }
}

const mapStateToProps = state => state;

const mapDispatchToProps = dispatch => {
  return {
    fetchUserCatchAction: bindActionCreators(fetchUserCatch, dispatch),
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(AddEditVolunteer)
