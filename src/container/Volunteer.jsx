import React, { Component } from 'react';
import VolunteerList from '../component/VolunteerList';
import { fetchUserCatch } from '../redux/actions/cache';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

export class Volunteer extends Component {

  componentDidMount() {
    this.props.fetchUserCatchAction.request();
  }

  render() {
    return (
      <div>
        <VolunteerList />
      </div>
    )
  }
}

const mapStateToProps = state => state;

const mapDispatchToProps = dispatch => {
  return {
    fetchUserCatchAction: bindActionCreators(fetchUserCatch, dispatch),
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Volunteer)
