import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import {
    TextField, withStyles, Button,
    Card, CardActions, Avatar, Dialog, DialogActions,
    DialogContent, DialogContentText, DialogTitle
} from '@material-ui/core';
import LockIcon from '@material-ui/icons/LockOutlined';
import { connect } from 'react-redux';
import { updateByPropertyName } from '../utils/utils';
import authService from '../service/auth';
import { userInfoSuccess, fetchAuthUserSuccess } from '../redux/actions/current-user';
import awsConfig from '../utils/aws-config';
import * as Routes from '../routes/path';

const styles = theme => ({
    main: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
        alignItems: 'center',
        justifyContent: 'flex-start',
        background: 'url(https://source.unsplash.com/random/1600x900?school)',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
    },
    card: {
        minWidth: 300,
        marginTop: '6em',
        padding: '3em'
    },
    avatar: {
        margin: '1em',
        display: 'flex',
        justifyContent: 'center',
    },
    icon: {
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        padding: '0 1em 1em 1em',
    },
    input: {
        marginTop: '1em',
    },
    actions: {
        padding: '0 1em 1em 1em',
    },
    errorMessage: {
        marginTop: '1em',
        display: 'flex',
        justifyContent: 'center',
        color: theme.palette.grey[500],
    },
    header : {
        justifyContent : 'center',
        textAlign : 'center',
        fontSize : 20
    }
});

const renderInput = ({
    meta: { touched, error } = {},
    input: { ...inputProps },
    ...props
}) => (<TextField
    error={!!(touched && error)}
    helperText={touched && error}
    {...inputProps}
    {...props}
    fullWidth
/>);


const INITIAL_STATE = {
    userName: '',
    password: '',
    errorMessage: '',
    open: false,
    email: ''
}

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = INITIAL_STATE
    }
    onTextChange = (event) => {
        const { name, value } = event.target;
        this.setState(updateByPropertyName(name, value))
    }
    onFormSubmit = async (data) => {
        const { dispatch, history } = this.props;
        try {
            const { userName, password } = data;
            const authResponse = await authService.authenticateUserApi(userName, password);
            if (authResponse.signInUserSession) {
                const { signInUserSession } = authResponse;
                dispatch(fetchAuthUserSuccess(authResponse))
                localStorage.setItem('hmUser', JSON.stringify(signInUserSession))
                const userResponse = await authService.getUserDetail();
                if (userResponse.Username) {
                    dispatch(userInfoSuccess(userResponse))
                    history.push(Routes.ROOT);
                }
            } else {
                this.setState(updateByPropertyName('errorMessage', authResponse.message));
                throw authResponse.message
            }
        } catch (error) {

        }
    }
    handleClickOpenModel = () => {
        this.setState({ open: true, email: '' });
    };
    handleCloseModel = () => {
        this.setState({ open: false });
    };

    resetPassword = (event) => {
        event.preventDefault();
        const { email } = this.state;
        authService.resetPassword(email);
        this.setState({ open: false })
    }

    render() {
        const { classes, handleSubmit } = this.props;
        const { errorMessage, email } = this.state;
        return (
            <div className={classes.main}>
                <Card className={classes.card}>
                <div className={classes.header}>{awsConfig.app_header}</div>
                    <div className={classes.avatar}>
                        <Avatar className={classes.icon}>
                            <LockIcon />
                        </Avatar>
                    </div>
                    <form onSubmit={handleSubmit(this.onFormSubmit)}>
                        <div className={classes.errorMessage}> {errorMessage}</div>
                        <div className={classes.form}>
                            <div className={classes.input}>
                                <Field
                                    name="userName"
                                    component={renderInput}
                                    label="Username"
                                />
                            </div>
                            <div className={classes.input}>
                                <Field
                                    name="password"
                                    component={renderInput}
                                    label="Password"
                                    type="password"
                                />
                            </div>
                        </div>
                        <CardActions className={classes.actions}>
                            <Button
                                variant="contained"
                                type="submit"
                                color="primary"
                                className={classes.button}
                                fullWidth
                            >
                                SIGN IN
                            </Button>
                        </CardActions>
                    </form>
                    <div>
                        <button className="btn btn-link" onClick={this.handleClickOpenModel}>Forget Password</button>
                    </div>
                </Card>

                <Dialog
                    open={this.state.open}
                    onClose={this.handleCloseModel}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Forgot Password</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            To reset password, please enter your email address here. please check your email.
                    </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            value={email}
                            name="email"
                            label="Email Address"
                            type="email"
                            onChange={this.onTextChange}
                            fullWidth
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCloseModel} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.resetPassword} color="primary">
                            Reset Password
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

const loginForm = reduxForm({
    form: 'loginForm',
    enableReinitialize: true,
})(Login)

const mStyles = withStyles(styles)(loginForm);

const mapStateToProps = state => ({ state });

export default connect(mapStateToProps)(mStyles);

