import React from 'react';
import { Route } from 'react-router-dom';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Badge from '@material-ui/core/Badge';
import NotificationsIcon from '@material-ui/icons/Notifications';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuList from '@material-ui/core/MenuList';
import { setClassList } from '../redux/actions/attendance';

import * as  routes from '../routes/path';
import AdminRoutes from '../routes/AdminRoutes';
import { userInfoSuccess, fetchAuthUserSuccess } from '../redux/actions/current-user';

import authService from '../service/auth';
import OtherRoutes from '../routes/OtherRoutes';


const ITEM_HEIGHT = 48;
const drawerWidth = 240;
const options = [
    'None',
    'Atria',
    'Callisto',
    'Dione',
    'Ganymede',
    'Hangouts Call',
    'Luna',
    'Oberon',
    'Phobos',
    'Pyxis',
    'Sedna',
    'Titania',
    'Triton',
    'Umbriel',
];

const styles = theme => ({
    root: {
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    header: {
        flexGrow: 1,
        font: '700 15px/28px "Lato", sans-serif',
        textTransform: 'uppercase',
    },
    rightContainer: {
        marginRight: 12,
    },
    smallAvatar: {
        width: 30,
        height: 30,
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 36,
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing.unit * 7 + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing.unit * 9 + 1,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 3,
    },
});

const INITIAL_STATE = {
    openDrawer: false,
    anchorElProfileMenu: null,
    anchorElNotificationMenu: null
}

class Layout extends React.Component {
    constructor(props) {
        super(props);
        this.state = INITIAL_STATE;
    }

    async componentWillMount() {
        const { dispatch } = this.props;
        const signInUserSession = JSON.parse(localStorage.getItem('hmUser'));
        if (signInUserSession) {
            dispatch(fetchAuthUserSuccess(signInUserSession));
            const userResponse = await authService.getUserDetail();
            if (userResponse.Username) {
                const classList = this.convertGroupsObjectToArray(userResponse.Groups);
                // dispatch(fetchUserCacheRequest());
                dispatch(setClassList(classList));
                dispatch(userInfoSuccess(userResponse));
            }
        }
    }

    convertGroupsObjectToArray(Groups = {}) {
        const classList = [];
        for (const key in Groups) {
            if (Groups.hasOwnProperty(key)) {
                const element = Groups[key];
                classList.push(element)
            }
        }
        return classList;
    }

    handleDrawer = () => {
        this.setState((prevState) => ({ openDrawer: !prevState.openDrawer }));
    };

    handleProfileMenu = event => {
        this.setState({ anchorElProfileMenu: event.currentTarget });
    };

    handleProfileClose = () => {
        this.setState({ anchorElProfileMenu: null });
    };

    handleNotificationMenu = event => {
        this.setState({ anchorElNotificationMenu: event.currentTarget });
    };

    handleNotificationClose = () => {
        this.setState({ anchorElNotificationMenu: null });
    };

    onClickLogout = (event) => {
        localStorage.removeItem('hmUser');
        this.props.history.push('/signin');
    }

    mainMenuListItems = (
        <div>
            <Link className="removeclass" to={routes.WELCOME}>
                <ListItem button>
                    <ListItemIcon>
                        <i className="material-icons"> subject </i>
                    </ListItemIcon>
                    <ListItemText primary="Post Feed" />
                </ListItem>
            </Link>

            <Link className="removeclass" to={routes.NEWSLETTER}>
                <ListItem button>
                    <ListItemIcon>
                        <i className="material-icons"> poll </i>
                    </ListItemIcon>
                    <ListItemText primary="News Letter" />
                </ListItem>
            </Link>

            <Link className="removeclass" to={routes.PORTFOLIO}>
                <ListItem button>
                    <ListItemIcon>
                        <i className="material-icons"> work </i>
                    </ListItemIcon>
                    <ListItemText primary="Portfolio" />
                </ListItem>
            </Link>
            <Link className="removeclass" to={routes.VOLUNTEER}>
                <ListItem button>
                    <ListItemIcon>
                        <i className="material-icons"> pan_tool </i>
                    </ListItemIcon>
                    <ListItemText primary="Volunteer" />
                </ListItem>
            </Link>

            {/* <Divider /> */}
        </div >
    );

            

    adminMenuListItems = (
        <div>
            <Link className="removeclass" to={routes.DASHBOARD}>
                <ListItem button>
                    <ListItemIcon>
                        <i className="material-icons"> school </i>
                    </ListItemIcon>
                    <ListItemText primary="School Profile" />
                </ListItem>
            </Link>

            <Link className="removeclass" to={routes.StudentSummary}>
                <ListItem button>
                    <ListItemIcon>
                        <i className="material-icons"> update </i>
                    </ListItemIcon>
                    <ListItemText primary="Attendance Summary" />
                </ListItem>
            </Link>
        </div >
    );

    render() {
        const { classes, currentUser: { auth, userInfo: { role } } } = this.props;
        const { anchorElProfileMenu, anchorElNotificationMenu, openDrawer } = this.state;
        const openProfileMenu = Boolean(anchorElProfileMenu);
        const openNotificationMenu = Boolean(anchorElNotificationMenu);

        return (
            <div className={classes.root}>
                <CssBaseline />
                <AppBar
                    position="fixed"
                    className={classNames(classes.appBar)}
                >
                    <Toolbar disableGutters={true}>

                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawer}
                            className={classNames(classes.menuButton)}
                        >
                            <MenuIcon />
                        </IconButton>

                        <Typography variant="h6" color="inherit" noWrap className={classes.header}>
                            Welcome to HMS 360
                        </Typography>

                        {auth.idToken &&
                            <div className={classes.rightContainer}>
                                <IconButton
                                    aria-owns={openNotificationMenu ? 'menu-appbar' : undefined}
                                    aria-haspopup="true"
                                    onClick={this.handleNotificationMenu}
                                    color="inherit">
                                    <Badge badgeContent={8} color="secondary">
                                        <NotificationsIcon />
                                    </Badge>
                                </IconButton>

                                <Popper
                                    open={openNotificationMenu}
                                    onClose={this.handleNotificationClose}
                                    anchorEl={anchorElNotificationMenu}
                                    transition
                                    disablePortal>
                                    {({ TransitionProps, placement }) => (
                                        <Grow
                                            {...TransitionProps}
                                            id="menu-list-grow"
                                            style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                                        >
                                            <Paper>
                                                <ClickAwayListener onClickAway={this.handleNotificationClose}>
                                                    <MenuList style={{
                                                        maxHeight: ITEM_HEIGHT * 4.5,
                                                        width: 200,
                                                        overflow: 'auto'
                                                    }
                                                    }>
                                                        {options.map(option => (
                                                            <MenuItem key={option} selected={option === 'Pyxis'} onClick={this.handleNotificationClose}>
                                                                {option}
                                                            </MenuItem>
                                                        ))}
                                                    </MenuList>
                                                </ClickAwayListener>
                                            </Paper>
                                        </Grow>
                                    )}
                                </Popper>


                                <IconButton
                                    aria-owns={openProfileMenu ? 'menu-appbar' : undefined}
                                    aria-haspopup="true"
                                    onClick={this.handleProfileMenu}
                                    color="inherit"
                                >
                                    <Avatar
                                        className={classes.smallAvatar}
                                        alt={auth.idToken.payload.given_name}
                                        src={auth.idToken.payload.picture}
                                    />
                                </IconButton>
                                <Popper
                                    id="menu-appbar"
                                    open={openProfileMenu}
                                    anchorEl={anchorElProfileMenu}
                                    onClose={this.handleProfileClose}
                                    transition
                                    disablePortal>
                                    {({ TransitionProps, placement }) => (
                                        <Grow
                                            {...TransitionProps}
                                            id="menu-list-grow"
                                            style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                                        >
                                            <Paper>
                                                <ClickAwayListener onClickAway={this.handleProfileClose}>
                                                    <MenuList>
                                                        <MenuItem onClick={this.handleProfileClose}>
                                                            {auth.idToken.payload.given_name}
                                                        </MenuItem>
                                                        <MenuItem onClick={this.onClickLogout}>LogOut</MenuItem>
                                                    </MenuList>
                                                </ClickAwayListener>
                                            </Paper>
                                        </Grow>
                                    )}
                                </Popper>
                            </div>
                        }

                    </Toolbar>
                </AppBar>
                <Drawer
                    variant="permanent"
                    className={classNames(classes.drawer, {
                        [classes.drawerOpen]: openDrawer,
                        [classes.drawerClose]: !openDrawer,
                    })}
                    classes={{
                        paper: classNames({
                            [classes.drawerOpen]: openDrawer,
                            [classes.drawerClose]: !openDrawer,
                        }),
                    }}
                    open={openDrawer}
                >
                    <div className={classes.toolbar}>

                    </div>
                    <Divider />
                    <List>
                        {this.mainMenuListItems}
                        <Divider />
                        {role === 'admin' && this.adminMenuListItems}
                    </List>

                </Drawer>

                <main className={classes.content}>
                    <div className={classes.toolbar} />
                    <Route component={OtherRoutes} path={routes.ROOT} />
                    {role === 'admin' && <Route component={AdminRoutes} path={routes.ADMIN} />}

                </main>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    currentUser: state.currentUser,
});

export default withStyles(styles)(connect(mapStateToProps)(Layout));