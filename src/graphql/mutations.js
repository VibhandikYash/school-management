import gql from "graphql-tag";

const mutations = {

    createEvent: gql(`
        mutation($input: CreateEventInput) {
            createEvent(
            input: $input
            ) {
            id
            title
            description
            location
            background_img
            inviteeType
            notify
            groups {
                groupId
                parents
            }
            eventOccurance {
                occurance
                recurringType
                occuranceDetails {
                    date
                    timeSlots {
                        startTime
                        endTime
                    }
                }
            }
            slots {
                title
	            requiredCount
	            comment
            }
            }
        }
    `)
}

export default mutations;