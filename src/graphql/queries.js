import gql from "graphql-tag";

const queries = {

    getAllEvents: gql(`
    query {
        listEvents(limit: 1000) {
            title
            description
            location
            background_img
            inviteeType
            notify
            groups {
                groupId
                parents
            }
            eventOccurance {
                occurance
                recurringType
                occuranceDetails {
                    date
                    timeSlots {
                        startTime
                        endTime
                    }
                }
            }
            slots {
                title
	            requiredCount
	            comment
            }
            }
        }
      }
    `)
}

export default queries;