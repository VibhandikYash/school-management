import React, {  Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';

import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';

const styles = theme => ({
    fab: {
        margin: theme.spacing.unit,
        position:"fixed",
        bottom:"10px",
        right:"10px"
      },
});

const CreatePostPanel = (props) => {
    const { classes, onClick } = props;
    return (
        <Fragment>
           <Fab color="secondary" aria-label="Edit" className={classes.fab} onClick={onClick} >
              <Icon>edit_icon</Icon>
            </Fab>
        </Fragment>
    )
}



const styleWrapper = withStyles(styles)(CreatePostPanel);

export default styleWrapper; 