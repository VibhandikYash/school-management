import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import classNames from 'classnames';
import Chip from '@material-ui/core/Chip';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, DateTimePicker } from 'material-ui-pickers';
import Moment from 'react-moment';
import Tooltip from '@material-ui/core/Tooltip';
import Avatar from '@material-ui/core/Avatar';
import CircularProgress from '@material-ui/core/CircularProgress';
import blue from '@material-ui/core/colors/blue';
import AccessTime from '@material-ui/icons/AccessTime';
import LocalMovies from '@material-ui/icons/LocalMovies';
import CloseIcon from '@material-ui/icons/Close';
import NativeSelect from '@material-ui/core/NativeSelect';
import BootstrapInput from '../common/BootstrapInput';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    createPost
} from '../../redux/actions/feed-post';
import { handleTextChange, uploadImage, uploadVideo, 
    removeSelectedImage, removeSelectedVideo, handleCloseDialog } from '../../redux/actions/post-manage';
import { Dialog } from '@material-ui/core';

const propTypes = {
    classes: PropTypes.object.isRequired,
};

const styles = theme => ({
    card: {
        backgroundColor: '#f6f6f6',
        maxWidth: 650,
        // marginBottom: theme.spacing.unit * 2,
    },
    input: {
        margin: theme.spacing.unit,
    },
    formControl: {
        marginRight: theme.spacing.unit,
        marginLeft: theme.spacing.unit,
        minWidth: 60,
    },
    textAreaStyle: {
        backgroundColor: '#fff',
        padding: '15px 20px',
        width: '100%'
    },
    chip: {
        margin: theme.spacing.unit,
    },
    imageChip: {
        margin: theme.spacing.unit,
        height: '50px',
        borderRadius: '25px'
    },
    imagePreviewChip: {
        height: '50px',
        width: '50px'
    },
    fab: {
        margin: theme.spacing.unit * 2,
    },
    absolute: {
        position: 'absolute',
        bottom: theme.spacing.unit * 2,
        right: theme.spacing.unit * 3,
    },
    root: {
        display: 'flex',
        alignItems: 'center',
    },
    wrapper: {
        margin: theme.spacing.unit,
        position: 'relative',
    },
    buttonSuccess: {
        backgroundColor: blue[500],
        '&:hover': {
            backgroundColor: blue[700],
        },
    },
    fabProgress: {
        color: blue[500],
        position: 'absolute',
        top: -14,
        left: -1,
        zIndex: 1,
    },
    buttonProgress: {
        color: blue[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    },
    postFooter: {
        display: 'flex',
        justifyContent: 'space-between',
        paddingTop: '0px',
        paddingBottom: '0px'
    },
    selectBoxStyle: {
        backgroundColor: '#e2e2e2',
        borderRadius: '5px',
    }
});

class PostManageDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedDate: null,
            postLoading: false,
        };
    }


    handleDateChange = date => {
        this.setState({ selectedDate: date });
    };

    onDropImage = async (files) => {
        this.props.uploadImageAction.request(files);
    }

    onDropVideo = async (files) => {
        this.props.uploadVideoAction.request(files);
    }

    onHandleTextChange = (event) => {
        const { name, value } = event.target;
        this.props.handleTextChangeAction(name, value)
    }

    openPicker = e => {
        // do not pass Event for default pickers
        this.picker.open(e);
    };

    deleteScheduledTime = (e) => {
        this.setState({ selectedDate: null });
    }

    createPost = event => {
        event.preventDefault();
        const { createPostAction } = this.props;
        createPostAction.request();
    }


    render() {
        const { classes, userGroups, postForm } = this.props;
        const { selectedDate, postLoading } = this.state;
        const { groupids, text } = postForm;

        const images = postForm.images.map((image, index) => (
            <Chip
                key={image.name}
                className={classes.imageChip}
                avatar={<Avatar alt="avatar" className={classes.imagePreviewChip} src={image.preview} />}
                onDelete={() => { this.props.removeSelectedImageAction(index) }}
                variant="outlined"
            />
        ))

        const videos = postForm.videos.map((video, index) => (
            <Chip
                key={video.name}
                icon={<LocalMovies />}
                label={video.name}
                onDelete={() => { this.props.removeSelectedVideoAction(index) }}
                className={classes.chip}
                variant="outlined"
            />
        ))

        return (
            <Dialog
                fullWidth={true}
                maxWidth="sm"
                open={postForm.showDialog}
                onClose={this.props.closeDialogAction}
                aria-labelledby="form-dialog-title"
            >
                <Grid>
                    <Grid className={classes.card}>
                        <Card
                            style={{ backgroundColor: '#f6f6f6' }}
                            className="w-full overflow-hidden"
                        >
                            <Grid container direction="row" justify="space-between" >
                                <Grid container item style={{ padding: '6px 13px' }} xs={10}>
                                    <Grid item>
                                        <Typography variant="subtitle1" style={{ marginTop: '3px', marginRight: '3px' }}>
                                            What's new in
                                </Typography>
                                    </Grid>

                                    <Grid item >
                                        <NativeSelect
                                            value={groupids}
                                            onChange={this.onHandleTextChange}
                                            input={<BootstrapInput name="groupids" id="groupids-customized-native-simple" />}
                                        >
                                            <option value="" />
                                            {!!userGroups.length &&
                                                userGroups.map(group =>
                                                    (
                                                        <option key={group.groupid} value={group.groupid}>
                                                            {group.GroupName}
                                                        </option>
                                                    ))
                                            }
                                        </NativeSelect>
                                    </Grid>
                                </Grid>

                                <Grid item>
                                    <IconButton aria-label="Close" onClick={this.props.closeDialogAction} >
                                        <CloseIcon />
                                    </IconButton>
                                </Grid>

                            </Grid>

                            <Input
                                value={text}
                                onChange={this.onHandleTextChange}
                                className={classNames("w-full", classes.textAreaStyle)}
                                name="text"
                                placeholder="Write something.."
                                multiline
                                rows="4"
                                margin="none"
                                disableUnderline
                            />
                            {images}
                            <br />
                            {videos}

                            {selectedDate &&
                                (< Chip
                                    icon={<AccessTime />}
                                    label={<Moment
                                        format="DD-MM-YYYY HH:mm"
                                        date={this.state.selectedDate}
                                    />}
                                    onDelete={this.deleteScheduledTime}
                                    className={classes.chip}
                                    variant="outlined"
                                />)
                            }

                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <Grid container className={classes.grid} justify="space-around">
                                    <DateTimePicker
                                        style={{ display: 'none' }}
                                        ref={node => { this.picker = node; }}
                                        value={selectedDate}
                                        onChange={this.handleDateChange}
                                    />
                                </Grid>
                            </MuiPickersUtilsProvider>
                            <Grid
                                container
                                direction="row"
                                justify="space-between"
                                style={{
                                    paddingTop: '0px',
                                    paddingBottom: '0px'
                                }}
                                position="static"
                                color="default"
                                elevation={0}>

                                <div className="flex-1 items-center">
                                    <div className={classes.wrapper} style={{ display: 'inline' }}>
                                        <Dropzone
                                            accept="image/jpeg"
                                            onDrop={this.onDropImage}>
                                            {({ getRootProps, getInputProps, isDragActive }) =>
                                                (
                                                    <Fragment>
                                                        <Tooltip title="Upload Image">
                                                            <IconButton {...getRootProps()}
                                                                className={
                                                                    classNames('dropzone',
                                                                        { 'dropzone--isActive': isDragActive })
                                                                } aria-label="Add photo">
                                                                <input {...getInputProps()} />
                                                                <Icon>insert_photo</Icon>
                                                            </IconButton>
                                                        </Tooltip>
                                                    </Fragment>
                                                )
                                            }
                                        </Dropzone>
                                        {postForm.imageLoading && <CircularProgress size={50} className={classes.fabProgress} />}
                                    </div>

                                    <div className={classes.wrapper} style={{ display: 'inline' }}>
                                        <Dropzone
                                            accept="video/mp4"
                                            onDrop={this.onDropVideo}>
                                            {({ getRootProps, getInputProps, isDragActive }) =>
                                                (
                                                    <Fragment>
                                                        <Tooltip title="Upload video">
                                                            <IconButton {...getRootProps()}
                                                                className={
                                                                    classNames('dropzone',
                                                                        { 'dropzone--isActive': isDragActive })
                                                                } aria-label="Add photo">
                                                                <input {...getInputProps()} />
                                                                <Icon>videocam</Icon>
                                                            </IconButton>
                                                        </Tooltip>
                                                    </Fragment>
                                                )
                                            }
                                        </Dropzone>
                                        {postForm.videoLoading && <CircularProgress size={50} className={classes.fabProgress} />}
                                    </div>
                                    <Tooltip title="Schedule Post">
                                        <IconButton aria-label="Mention somebody" onClick={this.openPicker}>
                                            <Icon>access_time</Icon>
                                        </IconButton>
                                    </Tooltip>
                                    <Tooltip title="Pin Post">
                                        <IconButton aria-label="Add location">
                                            <Icon>beenhere</Icon>
                                        </IconButton>
                                    </Tooltip>
                                </div>

                                <div style={{ padding: '0.4em' }}>
                                    <NativeSelect
                                        value='onlyme'
                                        onChange={() => { }}
                                        input={<BootstrapInput name="postView" id="postView-customized-native-simple" />}
                                    >
                                        <option value="" />
                                        <option value='everyone'>EveryOne</option>
                                        <option value='onlyme'>Only Me</option>
                                        <option value='student'>Students</option>
                                        <option value='review'>Reviews</option>
                                        <option value='cancel'>Cancel</option>
                                    </NativeSelect>

                                    <div className={classes.wrapper} style={{ display: 'inline' }}>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            disabled={ groupids > 0 ? false : true}
                                            onClick={this.createPost}
                                        >
                                            POST
                                        </Button>
                                        {postLoading && <CircularProgress size={24} className={classes.buttonProgress} />}
                                    </div>
                                </div>
                            </Grid>
                        </Card>

                        <Divider className="my-32" />
                    </Grid>
                </Grid >
            </Dialog>

        )
    }
}



PostManageDialog.propTypes = propTypes
const styleWrapper = withStyles(styles)(PostManageDialog);

const mapStateToProps = state => ({
    currentUser: state.currentUser,
    postForm: state.postManage,
    userGroups: state.currentUser.userInfo.Groups ? Object.values(state.currentUser.userInfo.Groups) : [],
});

const mapDispatchToProps = dispatch => ({
    createPostAction: bindActionCreators(createPost, dispatch),
    handleTextChangeAction: bindActionCreators(handleTextChange, dispatch),
    uploadImageAction: bindActionCreators(uploadImage, dispatch),
    uploadVideoAction: bindActionCreators(uploadVideo, dispatch),
    removeSelectedImageAction: bindActionCreators(removeSelectedImage, dispatch),
    removeSelectedVideoAction: bindActionCreators(removeSelectedVideo, dispatch),
    closeDialogAction: bindActionCreators(handleCloseDialog, dispatch),
})

export default connect(mapStateToProps, mapDispatchToProps)(styleWrapper);