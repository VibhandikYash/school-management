import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';
import RecurringOptions from './RecurringOptions';
import moment from 'moment';
import CommonDialog from './CommonDialog';
import CommonTable from './CommonTable';
import Button from '@material-ui/core/Button';
import { RecurringOptions as RecurringOptionsEnum } from '../../../shared/enums';
import { connect } from 'react-redux';
import { compose } from 'redux';

import {
    setRecurringAddDay,
    setRecurringRemoveDay,
    setOccuranceType
} from "../../../redux/actions/volunteer";


import Slot from './Slot';

const styles = theme => ({
    root: {
        backgroundColor: theme.palette.background.paper,
    },
    chip: {
        marginRight: theme.spacing.unit,
    },
    section1: {
        margin: `${theme.spacing.unit}px ${theme.spacing.unit}px`,
    },
    section2: {
        margin: theme.spacing.unit * 2,
    },
    section3: {
        margin: `${theme.spacing.unit * 6}px ${theme.spacing.unit * 2}px ${theme.spacing.unit * 2}px`,
    },
    timePicker: {
        padding: theme.spacing.unit * 2,
    },
    recurringSection: {
        paddingLeft: theme.spacing.unit * 2,
    },
    selectionSection: {
        borderRight: '1px solid #0093AF',
    },
});


class TimeSlot extends Component {

    state = {
        startDate: null,
        endDate: null,
        recurringSelection: RecurringOptionsEnum.repeatEveryMonthSameNumericDay,
        week: [],
        dates: [],
        day: [],
    };

    handleChangeRecurring = key => (event, value) => {
        this.setState({
            [key]: parseInt(value, 10),
        });
        this.props.setOccuranceType(parseInt(value, 10))
    };

    handleChange = name => event => {
        this.setState({ [name]: event.target.checked });
    };

    handleDialog = () => {
        const tempDate = this.calculateDates();
        const dates = tempDate[0];
        const day = tempDate[1];
        this.setState({
            open: true,
            dates,
            day
        });
    };


    calculateDates = () => {
        const { startDate, endDate, week, recurringSelection } = this.state;
        const momentStartDate = moment(startDate);
        const momentEndDate = moment(endDate);
        const dates = [];
        const day = [];
        const weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        if (recurringSelection === RecurringOptionsEnum.repeatEveryWeek) {
            for (let m = moment(momentStartDate); m.diff(momentEndDate, 'days') <= 0; m.add(1, 'days')) {
                if (week.includes(m.isoWeekday().toString())) {
                    dates.push(m.toDate());
                    day.push(weekday[m.toDate().getDay()]);
                }
            }
        } else if (recurringSelection === RecurringOptionsEnum.repeatEveryMonthSameNumericDay) {
            for (let m = moment(momentStartDate); m.diff(momentEndDate, 'day') <= 0; m.add(1, 'month')) {
                if (m.isValid()) {
                    dates.push(m.toDate());
                    day.push(weekday[m.toDate().getDay()]);
                }
            }
        }
        return [dates, day];
    }

    handleDateChange = (dateType, value) => {
        this.setState({ [dateType]: value });
    }

    handleClose = () => {
        this.setState({ open: false });
    }

    handleWeekDayChange = weekDay => ev => {
        ev.stopPropagation();
        if (ev.target.checked) {
            this.props.setRecurringAddDay(weekDay);
            this.setState(prevState => {
                const week = [...prevState.week, weekDay];
                return { ...prevState, week };
            });
        } else {
            this.props.setRecurringRemoveDay(weekDay);
            this.setState(prevState => {
                const week = [...prevState.week.filter(day => day !== weekDay)];
                return { ...prevState, week };
            });
        }
    };

    renderWeekDays = () => {
        const { classes } = this.props;
        if (this.state.recurringSelection === 'repeatEveryWeek') {
            return (
                <FormGroup row>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.state.week}
                                onChange={this.handleChange('su')}
                                value="su"
                                color="primary"
                            />
                        }
                        label="SU"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.state.week}
                                onChange={this.handleChange('mo')}
                                value="mo"
                                color="primary"
                            />
                        }
                        label="MO"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.state.week}
                                onChange={this.handleChange('tu')}
                                value="tu"
                                color="primary"
                            />
                        }
                        label="TU"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.state.week}
                                onChange={this.handleChange('we')}
                                value="we"
                                color="primary"
                            />
                        }
                        label="WE"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.state.week}
                                onChange={this.handleChange('th')}
                                value="th"
                                color="primary"
                            />
                        }
                        label="TH"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.state.week}
                                onChange={this.handleChange('fr')}
                                value="fr"
                                color="primary"
                            />
                        }
                        label="FR"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.state.week}
                                onChange={this.handleChange('sa')}
                                value="sa"
                                color="primary"
                                classes={{
                                    root: classes.root,
                                    checked: classes.checked,
                                }}
                            />
                        }
                        label="SA"
                    />
                </FormGroup>
            );
        }
    };

    render() {
        const { classes, event } = this.props;
        const { recurringSelection, day, dates, endDate } = this.state;
        return (
            <div className={classes.root}>
                <div className={classes.section1}>
                    <Typography color="textSecondary">
                        Takes place at one location on recurring days (i.e. snacks every Sunday)
                    </Typography>
                </div>
                <Divider variant="middle" />
                <div className={classes.section2}>
                    <div>
                        <Grid container>
                            <Grid item xs={8} className={classes.selectionSection}>
                                <Slot multipleSlots={true} index={0} event={event}/>
                            </Grid>
                            <Grid item xs={4} className={classes.recurringSection}>
                                <div>
                                    <Grid container justify="flex-start">
                                        <Grid item xs={12}>
                                            <Typography color="primary" variant="h6">
                                                Recurring
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <RecurringOptions
                                                recurringSelection={recurringSelection}
                                                onRecurringOptionChange={this.handleChangeRecurring}
                                                onWeekDayChange={this.handleWeekDayChange}
                                                selectedDays={this.state.week}
                                            />
                                            <Button variant="contained" color="primary" onClick={this.handleDialog}>Review
                                                Dates</Button>
                                            <CommonDialog
                                                classes={{
                                                    paper: classes.paper,
                                                }}
                                                title="Select Time Slot"
                                                customComponent={<div><CommonTable startDates={dates} day={day}
                                                    endDate={endDate} /></div>}
                                                open={this.state.open}
                                                onClose={this.handleClose}
                                                value={this.state.value}
                                            />
                                            {/* <FormControl component="fieldset">
                                                <RadioGroup
                                                    column
                                                    name="recurringSelection"
                                                    aria-label="recurringSelection"
                                                    value={recurringSelection}
                                                    onChange={this.handleChangeRecurring('recurringSelection')}>
                                                    <FormControlLabel value="repeatEveryWeek" control={<Radio color="primary" />}
                                                        label="Repeat every week on the days" />
                                                    {this.renderWeekDays()}
                                                    <FormControlLabel value="repeatEveryMonthSameNumericDay"
                                                        control={<Radio color="primary" />}
                                                        label="Repeat every month on the same numeric day as the start day (i.e. the 12th of every month)" />
                                                    <FormControlLabel value="repeatEveryMonthSameRelativeDay"
                                                        control={<Radio color="primary" />}
                                                        label="Repeat every month on the same relative day as the start day (i.e. the first Sunday of every month)" />
                                                </RadioGroup>
                                            </FormControl> */}
                                        </Grid>
                                    </Grid>
                                </div>
                            </Grid>
                        </Grid>
                    </div>
                </div>
            </div>
        );
    }
}

TimeSlot.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state, props) => {
    const { event } = state.volunteers;
    console.log("in timeslot map state", event, props);
    return {
        event: state.volunteers.event
    }
};


const mapDispatchToProps = dispatch => {
    return {
        setOccuranceType: (payload) => dispatch(setOccuranceType(payload)),
        setRecurringAddDay: (payload) => dispatch(setRecurringAddDay(payload)),
        setRecurringRemoveDay: (payload) => dispatch(setRecurringRemoveDay(payload)),
    }
};

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(TimeSlot);