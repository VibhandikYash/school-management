import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import DateFnsUtils from '@date-io/date-fns';
import {MuiPickersUtilsProvider, TimePicker, DatePicker} from 'material-ui-pickers';
import {connect} from 'react-redux';
import moment from 'moment';

/*import MUIPlacesAutocomplete from 'mui-places-autocomplete';*/
// import TextField from '@material-ui/core/TextField';
import {setEventDate, setEventTime, setEventAddSlot} from "../../../redux/actions/volunteer";

const styles = theme => ({
    root: {
        backgroundColor: theme.palette.background.paper,
    },
    chip: {
        marginRight: theme.spacing.unit,
    },
    section1: {
        margin: `${theme.spacing.unit}px ${theme.spacing.unit}px`,
    },
    section2: {
        margin: theme.spacing.unit * 2,
    },
    section3: {
        margin: `${theme.spacing.unit * 6}px ${theme.spacing.unit * 2}px ${theme.spacing.unit * 2}px`,
    },
    timePicker: {
        padding: theme.spacing.unit * 2,
    }
});

class OneTime extends Component {
    state = {
        // The first commit of Material-UI
        selectedDate: new Date(),
        startTime: new Date(),
        endTime: new Date()
    };

    handleDateChange = date => {
        this.setState({selectedDate: date});
    };

    onSuggestionSelected(suggestion) {
        // Add your business logic here. In this case we just log...
        console.log('Selected suggestion:');
    }

    setTime = (timeType, value) => {
        const {
            setEventTime
        } = this.props;

        setEventTime({timeType, index: 0, time: moment(value).format('HH:mm'), timeIndex: 0})
    };

    setDate = (value) => {
        const {
            setEventDate
        } = this.props;
        setEventDate({index: 0, dateType: 'startDate', date: moment(value).format('YYYY-MM-DD')})
    };

    componentDidMount() {
        const {setEventDate, setEventAddSlot} = this.props;
        setEventDate({index: 0, dateType: 'startDate', date: ''});
        setEventAddSlot({index: 0});
    }

    render() {
        const {
            classes,
            slot
        } = this.props;
        const eventDate = slot.startDate ? new Date(slot.startDate) : new Date();
        const eventStartTime = slot.timeSlots && slot.timeSlots[0] && slot.timeSlots[0].startTime ? moment(slot.timeSlots[0].startTime, "HH:mm").toDate() : new Date();
        const eventEndTime = slot.timeSlots && slot.timeSlots[0] && slot.timeSlots[0].endTime ? moment(slot.timeSlots[0].endTime, "HH:mm").toDate() : new Date();
        return (
            <div className={classes.root}>
                <div className={classes.section1}>
                    <Typography color="textSecondary">
                        Takes place at one time at one location (i.e. a potluck or party)
                    </Typography>
                </div>
                <Divider variant="middle"/>
                <div className={classes.section2}>
                    <div>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <Grid container className={classes.grid} justify="flex-start">
                                <Grid item xs={12} className={classes.section1}>
                                    <Typography color="primary" variant="h6">
                                        Event Date
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} className={classes.section1}>
                                    <DatePicker
                                        variant='outlined'
                                        margin="none"
                                        label="Date of Event"
                                        value={eventDate}
                                        onChange={(date) => this.setDate(date)}
                                        format="dd/MM/yyyy"
                                        mask={value => (value ? [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/] : [])}
                                        clearable
                                        disablePast
                                    />
                                </Grid>
                                <Grid item xs={12} className={classes.section1}>
                                    <Typography color="primary" variant="h6">
                                        Event Time
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} className={classes.section1}>
                                    <Grid container className={classes.grid} justify="flex-start">
                                        <Grid item xs={6}>
                                            <TimePicker
                                                margin="none"
                                                label="Start Time"
                                                value={eventStartTime}
                                                onChange={(date) => this.setTime('startTime', date)}
                                                variant='outlined'
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TimePicker
                                                margin="none"
                                                label="End Time"
                                                value={eventEndTime}
                                                onChange={(date) => this.setTime('endTime', date)}
                                                variant='outlined'
                                            />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </MuiPickersUtilsProvider>
                    </div>
                </div>
                {/*<div className={classes.section2}>
                    <div>
                        <Grid container className={classes.grid} justify="flex-start">
                            <Grid item xs={6}>
                            {<MUIPlacesAutocomplete
                                onSuggestionSelected={this.onSuggestionSelected}
                            renderTarget={() => (<Grid />)}/>}
                            <TextField
                                        id="searchLocation"
                                        label="Search Location"
                                        margin="normal"
                                        className={classes.textField}
                                        variant="outlined" />
                            </Grid>
                        </Grid>
                    </div>
                </div>*/}
            </div>
        );
    }
}

const mapStateToProps = state => {
    const {event} = state.volunteers;
    console.log("event", event);
    return {
        slot: event.dateTime[0] || {}
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setEventDate: (payload) => dispatch(setEventDate(payload)),
        setEventTime: (payload) => dispatch(setEventTime(payload)),
        setEventAddSlot: (payload) => dispatch(setEventAddSlot(payload))
    }
};

OneTime.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(OneTime));