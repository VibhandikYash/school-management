import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import DateFnsUtils from '@date-io/date-fns';
import {MuiPickersUtilsProvider, TimePicker, DatePicker} from 'material-ui-pickers';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import {connect} from 'react-redux';
import {compose} from 'redux';
import moment from "moment";

import {
    setEventDate,
    setEventTime,
    setEventAddSlot,
    setEventRemoveSlot
} from "../../../redux/actions/volunteer";

const styles = theme => ({
    root: {
        backgroundColor: theme.palette.background.paper,
    },
    chip: {
        marginRight: theme.spacing.unit,
    },
    section1: {
        margin: `${theme.spacing.unit}px ${theme.spacing.unit}px`,
    },
    section2: {
        margin: theme.spacing.unit * 2,
    },
    section3: {
        margin: `${theme.spacing.unit * 6}px ${theme.spacing.unit * 2}px ${theme.spacing.unit * 2}px`,
    },
    timePicker: {
        padding: theme.spacing.unit * 2,
    }
});

class Slot extends Component {

    componentDidMount() {
        const {slot} = this.props;
        if (!slot.timeSlots || slot.timeSlots.length === 0) {
            const {setEventDate, index} = this.props;
            setEventDate({index, dateType: 'startDate', date: ''});
            setEventDate({index, dateType: 'endDate', date: ''});
            this.addSlot();
        }
    }

    handleTimeChange = (timeType, value, timeIndex) => {
        const {setEventTime, index} = this.props;
        setEventTime({timeType, index, time: moment(value).format('HH:mm'), timeIndex})
    };

    addSlot = () => {
        const {setEventAddSlot, index, slot} = this.props;
        let startTime = moment().format('HH:mm');
        let endTime = moment().format('HH:mm');
        if (slot.timeSlots && slot.timeSlots.length >= 1) {
            const lastSlot = slot.timeSlots[slot.timeSlots.length - 1];
            const lastStartTime = moment(lastSlot.startTime, "HH:mm");
            const lastEndTime = moment(lastSlot.endTime, "HH:mm");
            const duration = moment.duration(lastEndTime.diff(lastStartTime));
            startTime = lastSlot.endTime;
            endTime = moment(lastSlot.endTime, "HH:mm")
                .add(duration.asMinutes(), 'minutes')
                .format('HH:mm');
        }
        setEventAddSlot({index, startTime, endTime});
    };

    removeSlot = (timeIndex) => {
        const {setEventRemoveSlot, index} = this.props;
        setEventRemoveSlot({index, timeIndex});
    };

    renderSlots = () => {
        const {classes, slot, multipleSlots} = this.props;
        if (!slot.timeSlots) return null;

        return slot.timeSlots.map((time, index) => {
            const startTime = time.startTime ? moment(time.startTime, "HH:mm").toDate() : null;
            const endTime = time.endTime ? moment(time.endTime, "HH:mm").toDate() : null;

            return (
                <Grid container className={classes.grid} justify="flex-start" key={`SLOT_${index}`}
                      style={{marginTop: index > 0 ? '20px' : '0'}}>
                    <Grid item xs={5}>
                        <TimePicker
                            margin="none"
                            label="Start Time"
                            value={startTime}
                            onChange={(date) => this.handleTimeChange('startTime', date, index)}
                            variant='outlined'/>
                    </Grid>
                    <Grid item xs={5}>
                        <TimePicker
                            margin="none"
                            label="End Time"
                            value={endTime}
                            minDate={startTime}
                            onChange={(date) => this.handleTimeChange('endTime', date, index)}
                            variant='outlined'/>
                    </Grid>
                    {multipleSlots ?
                        <Grid item xs={2}>
                            {index === (slot.timeSlots.length - 1) && <Button onClick={this.addSlot}>
                                <Icon>add</Icon>
                            </Button>}
                            {index !== (slot.timeSlots.length - 1) && <Button onClick={() => this.removeSlot(index)}>
                                <Icon>delete</Icon>
                            </Button>}
                        </Grid> : null
                    }
                </Grid>
            )
        });
    };

    handleDateChange = (dateType, value) => {
        const {setEventDate, index} = this.props;
        setEventDate({index, dateType, date: moment(value).format('YYYY-MM-DD')});
    };

    render() {
        const {classes, slot, isError} = this.props;
        const startDate = slot.startDate ? new Date(slot.startDate) : null;
        const endDate = slot.endDate ? new Date(slot.endDate) : null;
        return (
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Grid container className={classes.grid} justify="flex-start">
                    <Grid item xs={12} className={classes.section1}>
                        <Typography color="primary" variant="h6">
                            Event Date
                        </Typography>
                    </Grid>
                    <Grid item xs={12} className={classes.section1}>
                        <Grid container className={classes.grid} justify="flex-start">
                            <Grid item xs={5}>
                                <DatePicker
                                    margin="none"
                                    label="Start date"
                                    value={startDate}
                                    onChange={(date) => this.handleDateChange('startDate', date)}
                                    format="dd/MM/yyyy"
                                    mask={value => (value ? [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/] : [])}
                                    clearable
                                    disablePast
                                    variant='outlined'/>
                            </Grid>
                            <Grid item xs={5}>
                                <DatePicker
                                    margin="none"
                                    label="End date"
                                    value={endDate}
                                    onChange={(date) => this.handleDateChange('endDate', date)}
                                    format="dd/MM/yyyy"
                                    mask={value => (value ? [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/] : [])}
                                    clearable
                                    disablePast
                                    variant='outlined'
                                    minDate={startDate}
                                    error={isError}/>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} className={classes.section1}>
                        <Typography color="primary" variant="h6">
                            Event Time
                        </Typography>
                    </Grid>
                    <Grid item xs={12} className={classes.section1}>
                        {this.renderSlots()}
                    </Grid>
                </Grid>
            </MuiPickersUtilsProvider>

        );
    }
}

Slot.defaultProps = {
    index: 0
};

Slot.propTypes = {
    classes: PropTypes.object.isRequired,
    index: PropTypes.number
};

const mapStateToProps = (state, props) => {
    const {event} = state.volunteers;
    console.log("in slot map state", event, props);
    return {
        slot: event.dateTime[props.index] || {}
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setEventDate: (payload) => dispatch(setEventDate(payload)),
        setEventTime: (payload) => dispatch(setEventTime(payload)),
        setEventAddSlot: (payload) => dispatch(setEventAddSlot(payload)),
        setEventRemoveSlot: (payload) => dispatch(setEventRemoveSlot(payload)),
    }
};

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(Slot);
