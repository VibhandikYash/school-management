import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import DateFnsUtils from '@date-io/date-fns';
import IconButton from '@material-ui/core/IconButton';
import Snackbar from '@material-ui/core/Snackbar';
import RemoveIcon from '@material-ui/icons/Cancel';
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';
import amber from '@material-ui/core/colors/amber';
import moment from 'moment';
import { connect } from 'react-redux';
import { updateEventStep } from '../../../redux/actions/volunteer';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    warning: {
        backgroundColor: amber[700],
    },
});

const weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];


class CommonTable extends React.Component {
    state = {
        open: false,
        message: ''
    };

    showNotification = message => {
        this.setState({
            open: true,
            message
        })
    }

    handleDateChange = (key, date, index) => {
        // const selectedDate = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
        // console.log(date);
        // let weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        // const filtereDates = this.state.data.filter(item => {
        //     const filterDate = `${item.startDate.getFullYear()}-${item.startDate.getMonth() + 1}-${item.startDate.getDate()}`;
        //     return selectedDate === filterDate;
        // });
        // let data = this.state.data;
        // if (filtereDates.length > 0) {
        //     data[index].isError = true;
        // } else {
        //     data[index][key] = date;
        // }
        // data[index].day = weekday[date.getDay()];
        // this.setState({ data: data });
        const { event: { eventDates = [] }, updateEventDates } = this.props;
        const isDuplicate = eventDates.some(evDate => moment(date).isSame(moment(evDate)));
        if (isDuplicate) {
            this.showNotification('Event with same date already exists. Please try another date!');
            return;
        }
        eventDates[index] = date;
        updateEventDates({ eventDates });
    };

    handleRemoveEventDay = index => {
        const { event: { eventDates = [] }, updateEventDates } = this.props;
        if (eventDates.length > 1) {
            eventDates.splice(index, 1);
            updateEventDates({ eventDates });
        } else {
            this.showNotification('You need atleast one event date!');
        }
    };

    render() {
        const { classes, event: { eventDates = [] } } = this.props;
        const { open, message } = this.state;
        return (
            <Paper className={classes.root}>
                <Snackbar
                    anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                    open={open}
                    // className={classes.warning}
                    onClose={() => this.setState({ open: false })}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id">{message}</span>}
                />
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>#</TableCell>
                            <TableCell align="center">Date</TableCell>
                            <TableCell align="center">Week Day</TableCell>
                            <TableCell align="center">Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {eventDates.map((row, index) => (
                            <TableRow key={index}>
                                <TableCell component="th" scope="row">
                                    {index}
                                </TableCell>
                                <TableCell align="center">
                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <DatePicker
                                            margin="normal"
                                            label="Date"
                                            value={row}
                                            onChange={(date) => this.handleDateChange('startDate', date, index)}
                                            format="dd/MM/yyyy"
                                            // mask={value => (value ? [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/] : [])}
                                            clearable
                                            disablePast
                                            variant='outlined'
                                        // error={row.isError}
                                        />
                                    </MuiPickersUtilsProvider>
                                </TableCell>
                                <TableCell align="center">{weekday[moment(row).day()]}</TableCell>
                                <TableCell align="center">
                                    <IconButton aria-label="Remove" onClick={() => this.handleRemoveEventDay(index)}>
                                        <RemoveIcon />
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Paper>
        )
    }
}

CommonTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
    return {
        event: state.volunteers.event
    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateEventDates: (data) => dispatch(updateEventStep(data))
    }
}

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(CommonTable));