import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import { WeekDays, RecurringOptions as RecurringOptionsEnum } from '../../../shared/enums';
// import RecurringOptions from './RecurringOptions';
// import moment from 'moment';
// import CommonDialog from './CommonDialog';
// import CommonTable from './CommonTable';
import Icon from '@material-ui/core/Icon';


import Slot from './Slot';

const styles = theme => ({
    root: {
        backgroundColor: theme.palette.background.paper,
    },
    section1: {
        margin: `${theme.spacing.unit}px ${theme.spacing.unit}px`,
    },
    section2: {
        margin: theme.spacing.unit * 2,
    },
    section3: {
        margin: `${theme.spacing.unit * 6}px ${theme.spacing.unit * 2}px ${theme.spacing.unit * 2}px`,
    },
    timePicker: {
        padding: theme.spacing.unit * 2,
    },
    recurringSection: {
        paddingLeft: theme.spacing.unit * 2,
    },
    selectionSection: {
        borderRight: '1px solid #0093AF',
    },
    removeEvent: {
        textAlign: 'right'
    }
});


class MultipleTimeSlot extends Component {

    state = {
        startDate: null,
        endDate: null,
        recurringSelection: RecurringOptionsEnum.repeatEveryMonthSameNumericDay,
        week: [],
        dates: [],
        day: [],
        events: [
            { startDate: null, endDate: null, isError: false }
        ],
        isError: false
    };

    defaultEvent = { startDate: new Date(), endDate: new Date(), isError: false };

    handleDialog = () => {
        // const tempDate = this.calculateDates();
        // const dates = tempDate[0];
        // const day = tempDate[1];
        // this.setState({
        //     open: true,
        //     dates,
        //     day
        // });
    };

    

    handleAddEvent = () => {
        this.setState({
            events: this.state.events.concat([this.defaultEvent])
        })
    };

    removeEvent = (index) => {
        const event = this.state.events;
        event.splice(index, 1);
        this.setState({ event });
    };

    renderEvents = () => {
        const { events } = this.state;
        const { classes } = this.props;
        return events.map((event, index) => {
            return (
                <div>
                    {index === 0 ? null : <Divider variant="middle" />}
                    {index === 0 ? null :
                        <Button className={classes.removeEvent} onClick={this.removeEvent}>
                            <Icon>delete</Icon>
                        </Button>
                    }
                    <Slot multipleSlots={true} index={index} />
                </div>
            )
        });
    };

    handleChangeRecurring = key => (event, value) => {
        this.setState({
            [key]: parseInt(value, 10),
        });
    };

    handleChange = name => event => {
        this.setState({ [name]: event.target.checked });
    };

    handleWeekDayChange = weekDay => ev => {
        ev.stopPropagation();
        if (ev.target.checked) {
            this.setState(prevState => {
                return { ...prevState, week: [...prevState.week, weekDay] }
            })
        } else {
            this.setState(prevState => {
                return { ...prevState, week: [...prevState.week.filter(day => day !== weekDay)] }
            });
        }
    };

    handleDateChange = (dateType, value) => {
        if (dateType === "endDate") {
            if (this.state.startDate < value) {
                this.setState({
                    [dateType]: value,
                    isError: false
                });
            } else {
                this.setState({ isError: true })
            }
        } else {
            this.setState({ [dateType]: value });
        }
    };

    handleClose = value => {
        this.setState({ open: false });
    };

    renderWeekDays = () => {
        // const { classes } = this.props;
        if (this.state.recurringSelection === "repeatEveryWeek") {
            return (
                <FormGroup row>
                    {Object.keys(WeekDays).map(weekDay => <Fragment>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={this.state.week.includes(weekDay)}
                                    onChange={this.handleWeekDayChange(weekDay)}
                                    // value="su"
                                    chec
                                    color="primary"
                                />
                            }
                            label={weekDay.toUpperCase()}
                        />
                    </Fragment>)
                    }
                </FormGroup>
            );
        }
    };

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <div className={classes.section1}>
                    <Typography color="textSecondary">
                        Takes place at one location on recurring days (i.e. snacks every Sunday)
                    </Typography>
                    <Button variant="contained" color="primary" onClick={this.handleAddEvent}>
                        Add Event
                    </Button>
                </div>
                <Divider variant="middle" />
                <div className={classes.section2}>
                    <div>
                        <Grid container>
                            <Grid item xs={12}>
                                {this.renderEvents()}
                            </Grid>
                        </Grid>
                    </div>
                </div>
            </div>
        );
    }
}

MultipleTimeSlot.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MultipleTimeSlot);