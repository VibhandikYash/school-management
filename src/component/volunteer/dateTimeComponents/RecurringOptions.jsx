import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';

import { WeekDays, RecurringOptions as RecurringOptionsEnum } from '../../../shared/enums';


const WeekDaysOptions = ({ selectedDays = [], onWeekDayChange }) => {
    return (
        <FormGroup row>
            {Object.keys(WeekDays).map(weekDay => <Fragment>
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={selectedDays.includes(weekDay)}
                            onChange={onWeekDayChange(weekDay)}
                            // value={weekDay}
                            color="primary"
                        />
                    }
                    label={WeekDays[weekDay].slice(0, 2).toUpperCase()}
                />
            </Fragment>)
            }
        </FormGroup>
    );
};

const RecurringOptions = ({ recurringSelection, selectedDays, onRecurringOptionChange, onWeekDayChange }) => {
    return (
        <FormControl component="fieldset">
            <RadioGroup
                column
                name="recurringSelection"
                aria-label="recurringSelection"
                value={recurringSelection}
                onChange={onRecurringOptionChange('recurringSelection')}>
                <FormControlLabel value={RecurringOptionsEnum.repeatEveryWeek} control={<Radio color="primary" />}
                    label="Repeat every week on the days" />
                {recurringSelection === RecurringOptionsEnum.repeatEveryWeek && <WeekDaysOptions selectedDays={selectedDays} onWeekDayChange={onWeekDayChange} />}
                <FormControlLabel value={RecurringOptionsEnum.repeatEveryMonthSameNumericDay}
                    control={<Radio color="primary" />}
                    label="Repeat every month on the same numeric day as the start day (i.e. the 12th of every month)" />
                <FormControlLabel value={RecurringOptionsEnum.repeatEveryMonthSameRelativeDay}
                    control={<Radio color="primary" />}
                    label="Repeat every month on the same relative day as the start day (i.e. the first Sunday of every month)" />
            </RadioGroup>
        </FormControl>
    );
};

RecurringOptions.propTypes = {
    recurringSelection: PropTypes.string,
    selectedDays: PropTypes.array,
    onRecurringOptionChange: PropTypes.func.isRequired,
    onWeekDayChange: PropTypes.func.isRequired
};

export default RecurringOptions;
