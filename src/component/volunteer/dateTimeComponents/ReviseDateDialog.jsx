import React, { Component } from 'react'
import { MuiPickersUtilsProvider, TimePicker, DatePicker } from 'material-ui-pickers';
import DateFnsUtils from '@date-io/date-fns';

export default class ReviseDateDialog extends Component {
    render() {
        const { dates = [] } = this.props;
        console.log(dates);
        return (
            // <div>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                {dates.map((date, key) => {
                    // console.log(date);
                    return (
                        <DatePicker
                        key={key}
                        margin="none"
                        label="Date"
                        value={date}
                        onChange={(date) => console.log('date', date)}
                        format="dd/MM/yyyy"
                        mask={value => (value ? [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/] : [])}
                        clearable
                        disablePast
                        variant='outlined'
                    />
                    )
                })}
            </MuiPickersUtilsProvider>
        )
    }
}
