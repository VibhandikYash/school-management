import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from 'material-ui-pickers';
/*import MUIPlacesAutocomplete from 'mui-places-autocomplete';*/
import Button from '@material-ui/core/Button';
import moment from 'moment';
import { connect } from 'react-redux';
import { compose } from 'redux';

import CommonDialog from './CommonDialog';
import CommonTable from './CommonTable';
import RecurringOptions from './RecurringOptions';
import Slot from './Slot';
import { RecurringOptions as RecurringOptionsEnum } from '../../../shared/enums';

import {
    setRecurringAddDay,
    setRecurringRemoveDay,
    setOccuranceType,
    updateEventStep
} from "../../../redux/actions/volunteer";

const styles = theme => ({
    root: {
        backgroundColor: theme.palette.background.paper,
    },
    chip: {
        marginRight: theme.spacing.unit,
    },
    section1: {
        margin: `${theme.spacing.unit}px ${theme.spacing.unit}px`,
    },
    section2: {
        margin: theme.spacing.unit * 2,
    },
    section3: {
        margin: `${theme.spacing.unit * 6}px ${theme.spacing.unit * 2}px ${theme.spacing.unit * 2}px`,
    },
    timePicker: {
        padding: theme.spacing.unit * 2,
    },
    selectionSection: {
        borderRight: '1px solid #0093AF',
    },
    recurringSection: {
        paddingLeft: theme.spacing.unit * 2,
    }
});

class Recurring extends Component {
    state = {
        startDate: null,
        endDate: null,
        recurringSelection: RecurringOptionsEnum.repeatEveryMonthSameNumericDay,
        week: [],
        dates: [],
        day: [],
        isError: false
    };

    handleDialog = () => {
        const tempDate = this.calculateDates();
        const dates = tempDate[0];
        const day = tempDate[1];
        this.setState({
            open: true,
            dates,
            day
        });
    };

    calculateDates = () => {
        const { startDate, endDate, week, recurringSelection } = this.state;
        const momentStartDate = moment(startDate);
        const momentEndDate = moment(endDate);
        const dates = [];
        const day = [];
        var weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        if (recurringSelection === 'repeatEveryWeek') {
            for (let m = moment(momentStartDate); m.diff(momentEndDate, 'days') <= 0; m.add(1, 'days')) {
                if (week.includes(m.isoWeekday().toString())) {
                    dates.push(m.toDate());
                    day.push(weekday[m.toDate().getDay()]);
                }
            }
        } else if (recurringSelection === 'repeatEveryMonthSameNumericDay') {
            for (let m = moment(momentStartDate); m.diff(momentEndDate, 'day') <= 0; m.add(1, 'month')) {
                if (m.isValid()) {
                    dates.push(m.toDate());
                    day.push(weekday[m.toDate().getDay()]);
                }
            }
        } else if (recurringSelection === 'repeatEveryMonthSameRelativeDay') {
            for (let m = moment(momentStartDate); m.diff(momentEndDate, 'day') <= 0; m.add(28, 'days')) {
                if (m.isValid())
                    dates.push(m.toDate());
            }
        }
        return [dates, day];
    };

    handleChangeRecurring = key => (event, value) => {
        this.setState({
            [key]: parseInt(value, 10),
        });
        this.props.setOccuranceType(parseInt(value, 10))
    };

    handleClose = value => {
        this.setState({ open: false });
    };

    handleChange = name => event => {
        this.setState({ [name]: event.target.checked });
    };

    handleWeekDayChange = weekDay => ev => {
        ev.stopPropagation();
        if (ev.target.checked) {
            console.log(weekDay);
            this.props.setRecurringAddDay(weekDay);
            this.setState(prevState => {
                const week = [...prevState.week, weekDay];
                return { ...prevState, week };
            })
        } else {
            this.props.setRecurringRemoveDay(weekDay);
            this.setState(prevState => {
                const week = [...prevState.week.filter(day => day !== weekDay)];
                return { ...prevState, week };
            });
        }
    };

    render() {
        const { classes, event } = this.props;
        // console.log('event dates', eventDates);
        // const { eventDates } = event;
        const { recurringSelection, endDate, dates, day } = this.state;
        return (
            <div className={classes.root}>
                <div className={classes.section1}>
                    {/*<Grid container alignItems="center">
                        <Grid item xs={12}>
                            <Typography gutterBottom variant="h5">
                                Recurring
                            </Typography>
                        </Grid>
                    </Grid>*/}
                    <Typography color="textSecondary">
                        Takes place at one location on recurring days (i.e. snacks every Sunday)
                    </Typography>
                </div>
                <Divider variant="middle" />
                <div className={classes.section2}>
                    <div>
                        <Grid container className={classes.grid}>
                            <Grid item xs={8} className={classes.selectionSection}>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <Grid container className={classes.grid} justify="flex-start">
                                        <Slot multipleSlots={false} index={0} event={event}/>
                                    </Grid>
                                </MuiPickersUtilsProvider>
                            </Grid>
                            <Grid item xs={4} className={classes.recurringSection}>
                                <div>
                                    <Grid container justify="flex-start">
                                        <Grid item xs={6}>
                                            <Typography color="primary" variant="h6">
                                                Recurring
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <RecurringOptions
                                                recurringSelection={recurringSelection}
                                                onRecurringOptionChange={this.handleChangeRecurring}
                                                onWeekDayChange={this.handleWeekDayChange}
                                                selectedDays={this.state.week} />
                                            <Button variant="contained" color="primary" onClick={this.handleDialog}>
                                                Review Dates
                                            </Button>
                                            <CommonDialog
                                                classes={{
                                                    paper: classes.paper,
                                                }}
                                                title="Select Time Slot"
                                                customComponent={<div>
                                                    <CommonTable startDates={dates} day={day}
                                                        endDate={endDate} /></div>}
                                                open={this.state.open}
                                                onClose={this.handleClose}
                                                value={this.state.value} />
                                        </Grid>
                                    </Grid>
                                </div>
                            </Grid>
                        </Grid>
                    </div>
                </div>
                {/*<div className={classes.section2}>
                    <div>
                        <Grid container className={classes.grid} justify="flex-start">
                            <Grid item xs={6}>
                                {<MUIPlacesAutocomplete
                                onSuggestionSelected={this.onSuggestionSelected}
                            renderTarget={() => (<Grid />)}/>}
                                <TextField
                                    id="searchLocation"
                                    label="Search Location"
                                    margin="normal"
                                    className={classes.textField}
                                    variant="outlined" />
                            </Grid>
                        </Grid>
                    </div>
                                </div>*/}
            </div>
        );
    }
}


Recurring.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state, props) => {
    return {
        event: state.volunteers.event,
        // eventDates: state.volunteers.event
    }
};


const mapDispatchToProps = dispatch => {
    return {
        setOccuranceType: (payload) => dispatch(setOccuranceType(payload)),

        setRecurringAddDay: (payload) => dispatch(setRecurringAddDay(payload)),
        setRecurringRemoveDay: (payload) => dispatch(setRecurringRemoveDay(payload)),
        updateStepData: (data) => dispatch(updateEventStep(data))
    }
};

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(Recurring);