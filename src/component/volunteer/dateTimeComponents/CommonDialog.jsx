import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid';

const styles = {
 
  rowC: {
    display: 'flex',
    flexDirection: 'row',
  },
  titleStyle: {
    marginLeft: '27%'
  }
};

class CommonDialog extends React.Component {
  constructor(props) {
    super();
    this.state = {
      value: props.value,
      option: [],
      groupSelection: '',
      name: [],
      groupName: '',
      parents: [],
      inputParentNames: [],
      inputGroupNames: [],
      checkedParents: [],
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({
        value: nextProps.value,
      });
    }
  }

  handleToggle = value => () => {
    const { checkedParents } = this.state;
    const currentIndex = checkedParents.indexOf(value);
    const newChecked = [...checkedParents];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    this.setState({
      checkedParents: newChecked,
    });
  };

  handleCancel = () => {
    this.props.onClose(this.props.value);
  };

  handleOk = () => {
    this.props.onClose(this.state.value);
  };

  handleChange = key => (event, value) => {
    this.setState({ [key]: value, });
  };

  handleSelectGroup = (groupName) => (event) => {
    let selectedGroups = this.props.groups.filter(group => groupName.indexOf(group.groupName) > -1);
    let parents = [];
    selectedGroups.forEach(group => {
      parents = parents.concat(group.parents);
    });
    this.setState({ [event.target.name]: event.target.value, parents });
    return;
  };

  render() {
    const { value, groups, classes, ...other } = this.props;
    return (
      <Dialog
        disableBackdropClick
        disableEscapeKeyDown
        maxWidth="md"
        onEntering={this.handleEntering}
        aria-labelledby="confirmation-dialog-title"
        fullWidth={true}
        {...other}
      >

        <DialogTitle id="confirmation-dialog-title">{this.props.title}</DialogTitle>
        <Divider variant="middle" />
        <DialogContent className={classes.dialogContent}>
            <Grid item xs={12}>
              {this.props.customComponent}
            </Grid>
        </DialogContent>
        <Divider variant="middle" />
        <DialogActions>
          <Button onClick={this.handleCancel} color="primary">
            Cancel
          </Button>
          <Button onClick={this.handleOk} color="primary">
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

CommonDialog.propTypes = {
  onClose: PropTypes.func,
  value: PropTypes.string,
};

export default withStyles(styles)(CommonDialog);