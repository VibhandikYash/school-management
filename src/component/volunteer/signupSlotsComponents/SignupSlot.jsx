import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    root: {
        backgroundColor: theme.palette.background.paper,
    },
    chip: {
        marginRight: theme.spacing.unit,
    },
    section1: {
        margin: `${theme.spacing.unit}px ${theme.spacing.unit}px`,
    },
    section2: {
        margin: theme.spacing.unit * 2,
    },
    section3: {
        margin: `${theme.spacing.unit * 6}px ${theme.spacing.unit * 2}px ${theme.spacing.unit * 2}px`,
    },
    timePicker: {
        padding: theme.spacing.unit * 2,
    },
    textField: {
        width: '80%',
    },
    selectField: {
        marginTop: '2%',
        width: '80%',
    },
});

class SignupSlot extends Component {

    defaultSlot = { title: "", requiredCount: 0, comment: "" };
    componentDidMount() {
        const { slots, updateSlots } = this.props;
        if (slots.length < 1) {
            console.log(slots.concat(Object.assign({}, this.defaultSlot)));
            updateSlots(slots.concat(Object.assign({}, this.defaultSlot)));
        }
    }

    handleChange = (name, index) => event => {
        const { updateSlots, slots } = this.props;
        slots[index][name] = event.target.value;
        updateSlots(slots);
    };

    addSlot = () => {
        const { slots, updateSlots } = this.props;
        const updatedSlots = slots.concat([Object.assign({}, this.defaultSlot)])
        updateSlots(updatedSlots);
    };

    removeSlot = (index) => {
        const { slots, updateSlots } = this.props;
        slots.splice(index, 1);
        updateSlots(slots);
    };

    renderSlots = () => {
        const { classes, slots } = this.props;
        // const { slots } = this.state;

        return slots.map((slot, index) => {
            return (
                <Grid container className={classes.grid} justify="flex-start" key={`SLOT_${index}`}
                    style={{ marginTop: index > 0 ? '20px' : '0' }}>
                    <Grid item xs={4}>
                        <TextField
                            id="signupTitle"
                            label="Title of Slot"
                            value={slot.title}
                            name="inputSignUpTitle"
                            margin="normal"
                            className={classes.textField}
                            onChange={this.handleChange("title", index)}
                            variant="outlined" />
                    </Grid>
                    <Grid item xs={3}>
                        <TextField
                            id="signupTitle"
                            label="#Wanted"
                            value={slot.requiredCount}
                            onChange={this.handleChange("requiredCount", index)}
                            name="inputWanted"
                            margin="normal"
                            type="number"
                            className={classes.textField}
                            variant="outlined" />
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            id="comment"
                            label="Help Comment"
                            value={slot.comment}
                            name="inputHelpComment"
                            margin="normal"
                            onChange={this.handleChange("comment", index)}
                            className={classes.textField}
                            variant="outlined" />
                    </Grid>
                    {this.props.multipleSlots ?
                        <Grid item xs={1} className={classes.selectField}>
                            {index === 0 && <Button onClick={this.addSlot}>
                                <Icon>add</Icon>
                            </Button>}
                            {index > 0 && <Button onClick={() => this.removeSlot(index)}>
                                <Icon>delete</Icon>
                            </Button>}
                        </Grid> : null
                    }
                </Grid>
            )
        });
    };

    render() {
        const { classes } = this.props;
        // const { classes, handleDateChange, startDate, endDate } = this.props;
        // const { startTime, endTime, recurringSelection } = this.state;
        return (
            <div>
                <div>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <Grid container className={classes.grid} justify="flex-start">
                            <Grid item xs={12} className={classes.section1}>
                                {this.renderSlots()}
                            </Grid>
                        </Grid>
                    </MuiPickersUtilsProvider>
                </div>
            </div>
        );
    }
}

SignupSlot.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SignupSlot);