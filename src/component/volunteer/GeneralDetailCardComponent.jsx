import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import GeneralDetails from './generalDetailsComponents/GeneralDetails';
import GroupParent from './generalDetailsComponents/GroupParents';
import NotificationAlert from './generalDetailsComponents/NotificationAlert';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import red from '@material-ui/core/colors/blue';
import CardHeader from '@material-ui/core/CardHeader';
import { connect } from 'react-redux';

import { setEventGroups, setEventGeneralDetails, updateEventStep, setNotificationType } from '../../redux/actions/volunteer';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },

    textField: {
        width: 50 * theme.spacing.unit,
    },
    rickTextEditor: {
        height: 50 * theme.spacing.unit,
        overflow: 'auto'
    },
    avatar: {
        backgroundColor: red[500],
    },
});


class GeneralDetailCardComponent extends Component {

    render() {
        const {
            classes, setEventGeneralDetails,
            setEventGroups,
            event, groups = [],
            updateEventStep
        } = this.props;

        return (
            <div className={classes.root}>
                <Grid container>
                    <Grid item xs={12} className={classes.control}>
                        <Grid container spacing={24}>
                            <Grid item xs={7}>
                                <Card className={classes.card}>
                                    <CardHeader
                                        avatar={
                                            <Avatar aria-label="Recipe" className={classes.avatar}>
                                                <i className="material-icons"> home </i>
                                            </Avatar>
                                        }
                                        title="General Details" />
                                    <CardContent>
                                        <GeneralDetails setEventGeneralDetails={setEventGeneralDetails} event={event} />
                                    </CardContent>
                                </Card>
                            </Grid>
                            <Grid item xs={5}>
                                <Grid container spacing={24}>
                                    <Grid item xs={12}>
                                        <Card className={classes.card}>
                                            <CardHeader
                                                avatar={
                                                    <Avatar aria-label="Recipe" className={classes.avatar}>
                                                        <i className="material-icons"> group_add </i>
                                                    </Avatar>
                                                }
                                                title="Add groups / parents" />
                                            <CardContent>
                                                <GroupParent groups={groups} updateEventStep={updateEventStep} setEventGeneralDetails={setEventGeneralDetails} setEventGroups={setEventGroups} event={event} />
                                            </CardContent>
                                        </Card>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Card className={classes.card}>
                                            <CardHeader
                                                avatar={
                                                    <Avatar aria-label="Recipe" className={classes.avatar}>
                                                        <i className="material-icons"> notifications_active </i>
                                                    </Avatar>
                                                }
                                                title="Notifications" />
                                            <CardContent>
                                                <NotificationAlert event={event} />
                                            </CardContent>
                                        </Card>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        event: state.volunteers.event
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setEventGroups: (groups) => dispatch(setEventGroups(groups)),
        setEventGeneralDetails: (name, value) => dispatch(setEventGeneralDetails({ name, value })),
        updateEventStep: (data) => dispatch(updateEventStep(data)),
        setNotificationType: (notificationType) => dispatch(setNotificationType(notificationType))
    }
};

GeneralDetailCardComponent.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(GeneralDetailCardComponent));