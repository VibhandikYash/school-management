import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import blue from '@material-ui/core/colors/blue';
import {connect} from 'react-redux';

import OneTime from './dateTimeComponents/OneTime';
import Recurring from './dateTimeComponents/Recurring';
import TimeSlot from './dateTimeComponents/TimeSlot';
import MultipleTimeSlot from './dateTimeComponents/MultipleTimeSlot';
import {Occurances} from '../../shared/enums';
import {setEventType} from "../../redux/actions/volunteer";

const styles = theme => ({
    root: {
        flexGrow: 1,
    },

    textField: {
        width: 50 * theme.spacing.unit,
    },
    rickTextEditor: {
        height: 50 * theme.spacing.unit,
        overflow: 'auto'
    },
    avatar: {
        backgroundColor: blue[500],
    },
    selectionSection: {
        borderRight: '1px solid #0093AF',
    },
    section2: {
        padding: theme.spacing.unit * 2,
    },
});

class DateTime extends Component {

    state = {
        dateTimeSelection: Occurances.onetime,
    };

    handleChange = (event, value) => {
        this.props.setEventType(parseInt(value, 10));
    };

    renderGroupSelectionOption = () => {
        const {
            event
        } = this.props;
        switch (event.occuranceType) {
            case Occurances.multipletimeslots:
                return <MultipleTimeSlot/>;
            case Occurances.recurring:
                return <Recurring/>;
            case Occurances.timeslots:
                return <TimeSlot/>;
            default:
                return <OneTime/>;
        }
    };

    render() {
        const {classes, event} = this.props;
        return (
            <div className={classes.root}>
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                        <Card className={classes.card}>
                            <CardHeader
                                avatar={
                                    <Avatar aria-label="Recipe" className={classes.avatar}>
                                        <i className="material-icons"> home </i>
                                    </Avatar>
                                }
                                title="Select Event type, Date & Time"/>
                            <CardContent>
                                <Grid container spacing={24}>
                                    <Grid item xs={3} className={[classes.selectionSection, classes.section2]}>
                                        <FormControl component="fieldset">
                                            {/*<FormLabel>What type of event are people signing up for?</FormLabel>*/}
                                            <RadioGroup
                                                column
                                                name="dateTimeSelection"
                                                aria-label="dateTimeSelection"
                                                value={event.occuranceType}
                                                onChange={this.handleChange}>
                                                <FormControlLabel value={Occurances.onetime}
                                                                  control={<Radio color="primary"/>} label="One Time"/>
                                                <FormControlLabel value={Occurances.recurring}
                                                                  control={<Radio color="primary"/>} label="Recurring"/>
                                                <FormControlLabel value={Occurances.timeslots}
                                                                  control={<Radio color="primary"/>}
                                                                  label="Time Slots"/>
                                                <FormControlLabel value={Occurances.multipletimeslots}
                                                                  control={<Radio color="primary"/>}
                                                                  label="Multiple Time Slots"/>
                                            </RadioGroup>
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={9}>
                                        {this.renderGroupSelectionOption()}
                                    </Grid>
                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>

                </Grid>
            </div>


        );
    }

}

const mapStateToProps = state => {
    return {
        event: state.volunteers.event
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setEventType: (occurrence) => dispatch(setEventType(occurrence)),
    }
};

DateTime.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(DateTime));