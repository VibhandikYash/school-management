import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import red from '@material-ui/core/colors/blue';
import { connect } from 'react-redux';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { Storage } from 'aws-amplify';
import { Occurances as OccurancesEnum } from '../../shared/enums';
import CommonTable from '../volunteer/previewComponents/CommonTable';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },

    textField: {
        width: 50 * theme.spacing.unit,
    },
    rickTextEditor: {
        height: 50 * theme.spacing.unit,
        overflow: 'auto'
    },
    avatar: {
        backgroundColor: red[500],
    },
    card: {
        display: 'flex',
        minHeight: '300px'
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: 400,
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
    },
    playIcon: {
        height: 38,
        width: 38,
    },
});


class PreviewComponent extends Component {

    state = {
        image: ''
    }

    componentDidMount() {
        this.getImage();
    }

    getImage = async () => {
        let img = await Storage.get(this.props.event.background_img, { level: 'public' });
        this.setState({
            image: img
        })
    }
    render() {
        const {
            classes,
            event,
        } = this.props;

        return (
            <div className={classes.root}>
                <Grid container>
                    <Grid item xs={12} className={classes.control}>
                        <Grid container spacing={24}>
                            <Grid item xs={12}>
                                <Card className={classes.card}>
                                    <CardMedia
                                        className={classes.cover}
                                        image={this.state.image}
                                        title="Live from space album cover"
                                    />
                                    <div className={classes.details}>
                                        <CardContent className={classes.content}>
                                            <Typography component="h5" variant="h5">
                                                Event Title: {event.title}
                                            </Typography>
                                            <Typography variant="subtitle1" color="textSecondary">
                                                {/*event.description*/}
                                            </Typography>
                                            <Typography variant="subtitle1" color="textSecondary">
                                                Location: {event.location}
                                            </Typography>
                                            <Typography variant="subtitle1" color="textSecondary">
                                                Event Type: {Object.keys(OccurancesEnum).find(key => OccurancesEnum[key] === event.occuranceType)}
                                            </Typography>
                                        </CardContent>
                                    </div>

                                </Card>
                            </Grid>
                            <Grid item xs={12}>
                                <Card className={classes.card}>
                                    <div className={classes.details}>
                                        <CardContent className={classes.content}>
                                            <Typography component="h5" variant="h5">
                                                Available Slot
                                            </Typography>
                                            <CommonTable event = {event}/>
                                        </CardContent>
                                    </div>

                                </Card>
                            </Grid>

                        </Grid>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        event: state.volunteers.event
    }
};


PreviewComponent.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(connect(mapStateToProps)(PreviewComponent));