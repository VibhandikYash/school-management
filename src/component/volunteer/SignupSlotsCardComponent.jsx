import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import blue from '@material-ui/core/colors/blue';
import { connect } from 'react-redux';
import SignupSlot from './signupSlotsComponents/SignupSlot';
import { updateEventStep } from '../../redux/actions/volunteer';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },

    textField: {
        width: 50 * theme.spacing.unit,
    },
    rickTextEditor: {
        height: 50 * theme.spacing.unit,
        overflow: 'auto'
    },
    avatar: {
        backgroundColor: blue[500],
    },
    selectionSection: {
        borderRight: '1px solid #0093AF',
    },
    section2: {
        padding: theme.spacing.unit * 2,
    },
});

class SignupSlotsCardComponent extends Component {


    // state = {
    //     dateTimeSelection: '',
    // };

    // handleChange = key => (event, value) => {
    //     this.setState({
    //         [key]: value,
    //     });
    // };

    render() {
        const { classes, updateSlots, event: { slots = [] } } = this.props;
        // const { dateTimeSelection } = this.state;
        return (

            <div className={classes.root}>
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                        <Card className={classes.card}>
                            <CardHeader
                                avatar={
                                    <Avatar aria-label="Recipe" className={classes.avatar}>
                                        <i className="material-icons"> home </i>
                                    </Avatar>
                                }
                                title="Slots/Reminders"
                            />
                            <CardContent>
                                <Grid container spacing={24}>
                                    <Grid item xs={12} className={[classes.section2]}>
                                        <SignupSlot updateSlots={updateSlots} slots={slots} multipleSlots={true} />
                                    </Grid>
                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </div>
        );
    }

}

const mapStateToProps = state => {
    return {
        event: state.volunteers.event
    }
};

const mapDispatchToProps = dispatch => {
    return {
        updateSlots: (slots) => dispatch(updateEventStep({ slots }))
    }
};

SignupSlotsCardComponent.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(SignupSlotsCardComponent));