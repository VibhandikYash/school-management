import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import GroupParentsDialog from './GroupParentsDialog';
import { connect } from 'react-redux';
import { GroupSelector } from '../../../redux/selector';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    control: {
        padding: theme.spacing.unit * 2,
    },
    textField: {
        width: '100%',
    },
    chip: {
        margin: theme.spacing.unit / 2,
    },
    parentOpenLink: {
        cursor: 'pointer'
    },
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

class GroupParents extends Component {

    state = {
        groupParentSelection: '',
        name: [],
        groupName: '',
        parents: [],
        inputParentNames: [],
        inputGroupNames: [],
        selectedParents: [],
        open: false,
    };

    handleChange = key => (event, value) => {
        const { setEventGeneralDetails } = this.props;
        let open = false;
        setEventGeneralDetails("groupParentSelection", value);
        if (value === 'parents')
            open = true;
        return this.setState({
            [key]: value,
            open
        });
    };

    openPopUp = () => {
        return this.setState({
            open: true
        });
    };

    handleSelectGroup = event => {
        const { setEventGroups, groups } = this.props;
        const selectedGroups = groups.filter(group => event.target.value.indexOf(group.GroupName) > -1);
        setEventGroups(selectedGroups.map(group => {
            return { groupId: group.groupid, groupName: group.GroupName }
        }));
    };

    handleClose = (value) => {
        this.setState({ open: false, value: value });
    };

    parentCallback = (dataFromChild) => {
        this.setState({
            selectedParents: dataFromChild
        });
        // this.state.selectedParents = dataFromChild;
    };

    renderGroupSelectionOption = () => {
        const { classes, event, groups, selectedGroups, updateEventStep } = this.props;
        if (event.groupParentSelection === 'parents') {
            return (
                <div>
                    <Typography onClick={this.openPopUp} component="h2" variant="title" gutterBottom className={classes.parentOpenLink}>
                        Number of selected parents : {this.state.selectedParents.length}
                    </Typography>
                    <GroupParentsDialog
                        classes={{
                            paper: classes.paper,
                        }}
                        groups={groups}
                        updateEventStep={updateEventStep}
                        open={this.state.open}
                        onClose={this.handleClose}
                        value={this.state.value}
                        callbackFromParent={this.parentCallback} />
                </div>
            );
        } else if (event.groupParentSelection === 'group') {
            return (
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                        <FormControl component='fieldset' variant='outlined'>
                            <InputLabel>Select Group</InputLabel>
                            <Select
                                multiple
                                value={selectedGroups.map(group => group.groupName)}
                                name='inputGroupNames'
                                label='Select group name'
                                onChange={this.handleSelectGroup}
                                className={classes.textField}
                                input={
                                    <OutlinedInput
                                        name='groupName'
                                        id='select-multiple-checkbox' />
                                }
                                renderValue={selected => selected.join(', ')}
                                MenuProps={MenuProps}>
                                {groups.map(group => (
                                    <MenuItem key={group.GroupName} value={group.GroupName}>
                                        <Checkbox checked={selectedGroups.some(s => s.groupName === group.GroupName)} />
                                        <ListItemText primary={group.GroupName} />
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                </Grid>
            );
        }
    };

    render() {
        const { classes, event } = this.props;

        return (
            <div className={classes.root}>
                <Grid container>
                    <Grid item xs={12} className={classes.control}>
                        <Grid container spacing={24}>
                            <Grid item xs={12}>
                                <FormControl component='fieldset'>
                                    <FormLabel>Name of your group/parents</FormLabel>
                                    <RadioGroup
                                        row
                                        name='groupParentSelection'
                                        aria-label='groupParentSelection'
                                        value={event.groupParentSelection}
                                        onChange={this.handleChange('groupParentSelection')}>
                                        <FormControlLabel value='group' control={<Radio color='primary' />}
                                            label='Select group' />
                                        <FormControlLabel value='parents' control={<Radio color='primary' />}
                                            label='Select parents' />
                                    </RadioGroup>
                                </FormControl>
                            </Grid>
                            {this.renderGroupSelectionOption()}
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

GroupParents.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
    return {
        groups: GroupSelector.getGroupInfoWithParents(state),
        selectedGroups: state.volunteers.event.groups,
        event: state.volunteers.event
    }
}

export default withStyles(styles)(connect(mapStateToProps)(GroupParents));