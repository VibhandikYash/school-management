import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import { connect } from 'react-redux';

import { setNotificationType } from '../../../redux/actions/volunteer';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    control: {
        padding: theme.spacing.unit * 2,
    },
    textField: {
        width: '100%',
    },
    chip: {
        margin: theme.spacing.unit / 2,
    },
    parentOpenLink: {
        cursor: 'pointer'
    },
});

// const ITEM_HEIGHT = 48;
// const ITEM_PADDING_TOP = 8;
// const MenuProps = {
//     PaperProps: {
//         style: {
//             maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
//             width: 250,
//         },
//     },
// };

class NotificationAlert extends Component {

    handleChange = key => (event, value) => {
        this.props.setNotificationType(value);
    };

    render() {
        const { classes, event } = this.props;

        return (
            <div className={classes.root}>
                <Grid container>
                    <Grid item xs={12} className={classes.control}>
                        <Grid container spacing={24}>
                            <Grid item xs={12}>
                                <FormControl component='fieldset'>
                                    <FormLabel>Select notification type</FormLabel>
                                    <RadioGroup
                                        row
                                        name='notificationSelection'
                                        aria-label='notificationSelection'
                                        value={event.notificationType}
                                        onChange={this.handleChange('notificationSelection')}>
                                        <FormControlLabel value='one' control={<Radio color='primary' />}
                                            label='One day ago' />
                                        <FormControlLabel value='two' control={<Radio color='primary' />}
                                            label='Two days ago' />
                                        <FormControlLabel value='three' control={<Radio color='primary' />}
                                            label='Three days ago' />
                                    </RadioGroup>
                                </FormControl>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        );
    }
}


NotificationAlert.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
    return {
        event: state.volunteers.event
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setNotificationType: (notificationType) => dispatch(setNotificationType(notificationType))
    }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(NotificationAlert));