import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import IconButton from '@material-ui/core/IconButton';
import RemoveIcon from '@material-ui/icons/Cancel';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Divider from '@material-ui/core/Divider';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import deepOrange from '@material-ui/core/colors/deepOrange';
import deepPurple from '@material-ui/core/colors/deepPurple';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import CheckCircle from '@material-ui/icons/CheckCircle';
import CheckCircleOutline from '@material-ui/icons/CheckCircleOutline';

const styles = {
    avatar: {
        margin: "10px 10px 10px 25px",
    },
    avatarLable: {
        margin: "10px 20px",
    },
    orangeAvatar: {
        margin: 10,
        color: '#fff',
        backgroundColor: deepOrange[500],
    },
    purpleAvatar: {
        margin: 10,
        color: '#fff',
        backgroundColor: deepPurple[500],
    },
    groupSection: {
        margin: '25px 0px',
    },
    noSpace: {
        margin: 0,
        padding: 0
    },
    selectionSection: {
        borderRight: '1px solid #0093AF',
    },
    title: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '10px',
    },
    scrollingWrapper: {
        display: 'flex',
        flexWrap: 'nowrap',
        overflowX: 'auto',
        minHeight: '120px',
    },
    card: {
        flex: '0 0 auto',
    },
    backgroundParents: {
        backgroundColor: "rgba(0, 0, 0, .05);",
    },
    rowC: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    dialogContent: {
        padding: 0,
        overflow: 'scroll'
    },
    titleStyle: {
        marginLeft: '27%'
    },
    checkBox: {
        color: 'rgb(73,80,183)',
        '&$checked': {
            color: 'rgb(73,80,183)',
        },
    },
    checked: {},
};

class GroupParentsDialog extends React.Component {
    constructor(props) {
        super();
        this.state = {
            value: props.value,
            option: [],
            groupSelection: '',
            name: [],
            GroupName: '',
            parents: [],
            inputParentNames: [],
            inputGroupNames: [],
            checkedParents: [],
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value !== this.props.value) {
            this.setState({
                value: nextProps.value,
            });
        }
    }

    handleToggle = value => () => {
        const { checkedParents } = this.state;
        const { updateEventStep } = this.props;
        const currentIndex = checkedParents.indexOf(value);
        const newChecked = [...checkedParents];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        console.log('handle toggle');

        updateEventStep({ parents: newChecked });
        this.props.callbackFromParent(newChecked);

        this.setState({
            checkedParents: newChecked,
        });
    };

    handleCancel = () => {
        this.props.onClose(this.props.value);
    };

    handleOk = () => {
        this.props.onClose(this.state.value);
    };

    handleChange = key => (event, value) => {
        this.setState({ [key]: value, });
    };

    handleSelectGroup = (GroupName) => (event) => {
        let selectedGroups = this.props.groups.filter(group => GroupName.indexOf(group.GroupName) > -1);
        let parents = [];
        selectedGroups.forEach(group => {
            parents = parents.concat(group.parents);
        });
        this.setState({ [event.target.name]: event.target.value, parents });
    };

    render() {
        const { value, groups, classes, ...other } = this.props;
        const { inputGroupNames } = this.state;
        return (
            <Dialog
                disableBackdropClick
                disableEscapeKeyDown
                maxWidth="md"
                onEntering={this.handleEntering}
                aria-labelledby="confirmation-dialog-title"
                fullWidth={true}
                {...other}>

                <DialogTitle id="confirmation-dialog-title">Group/Parents Selection</DialogTitle>
                <Divider variant="middle" />
                <Grid container justify="flex-start" alignItems="center" className={classes.scrollingWrapper}>
                    {groups.map(option => (
                        <div onClick={this.handleSelectGroup(option.GroupName)} className={classes.card}
                            value={inputGroupNames}
                            name="inputGroupNames">
                            <Avatar className={classes.avatar}>G</Avatar>
                            <Typography className={classes.avatarLable}>
                                {option.GroupName}
                            </Typography>
                        </div>
                    ))}
                </Grid>

                <Divider variant="middle" />
                <DialogContent className={classes.dialogContent}>
                    <div className={classes.rowC}>
                        <Grid item xs={6}>
                            <Typography color="primary" variant="h6" className={classes.titleStyle}>
                                List of Parents from Group
                            </Typography>
                            <List>
                                {this.state.parents.map(parent => (
                                    <ListItem key={parent.display_name} role={undefined} dense button
                                        onClick={this.handleToggle(parent.display_name)}>
                                        <ListItemAvatar>
                                            <Avatar>P</Avatar>
                                        </ListItemAvatar>
                                        <ListItemText primary={parent.display_name} />
                                        <Checkbox
                                            icon={<CheckCircleOutline />} checkedIcon={<CheckCircle />}
                                            classes={{
                                                root: classes.checkBox,
                                                checked: classes.checked,
                                            }}
                                            checked={this.state.checkedParents.indexOf(parent.display_name) !== -1}
                                            tabIndex={-1}
                                            disableRipple
                                        />
                                    </ListItem>
                                ))}
                            </List>
                        </Grid>
                        <Grid item xs={6} className={classes.backgroundParents}>
                            <Typography color="primary" variant="h6" className={classes.titleStyle}>
                                All selected parents
                            </Typography>
                            <List className={classes.root}>
                                {this.state.checkedParents.map(parent => (
                                    <ListItem key={parent} role={undefined} dense button>
                                        <ListItemAvatar>
                                            <Avatar>P</Avatar>
                                        </ListItemAvatar>
                                        <ListItemText primary={parent} />
                                        <ListItemSecondaryAction>
                                            <IconButton aria-label="Remove" onClick={this.handleToggle(parent)}>
                                                <RemoveIcon />
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                ))}
                            </List>
                        </Grid>
                    </div>
                </DialogContent>
                <Divider variant="middle" />
                <DialogActions>
                    <Button onClick={this.handleCancel} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.handleOk} color="primary">
                        Ok
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

GroupParentsDialog.propTypes = {
    onClose: PropTypes.func,
    value: PropTypes.string,
};

export default withStyles(styles)(GroupParentsDialog);