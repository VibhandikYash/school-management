import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import TextField from '@material-ui/core/TextField';
import RichTextEditor from 'react-rte';
import { Storage } from 'aws-amplify';

import { FilePond, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import '../css/GeneralDetails.css';
// import { setEventGeneralDetails } from "../../../redux/actions/volunteer";
// import Geosuggest from 'react-geosuggest';

registerPlugin(FilePondPluginImagePreview, FilePondPluginFileValidateType);

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    control: {
        padding: theme.spacing.unit * 2,
    },
    textField: {
        width: '100%',
    },
    rickTextEditor: {
        height: 50 * theme.spacing.unit,
        overflow: 'auto'
    },
    imageUpload: {
        paddingTop: theme.spacing.unit * 2,
    },
});

// const names = [
//     'Group 1',
//     'Group 2',
//     'Group 3',
//     'Group 4',
//     'Group 5',
//     'Group 6',
//     'Group 7',
//     'Group 8',
//     'Group 9',
//     'Group 10',
// ];

class GeneralDetails extends Component {

    state = {
        files: [],
        titleError: false,
        titleText: ''
    };

    componentDidMount() {
        // this.pond.setOptions({
        //     server: {
        //         process: this.processFile
        //     }
        // });
    }

    handleChange = event => {
        const { setEventGeneralDetails } = this.props;
        setEventGeneralDetails(event.currentTarget.name, event.currentTarget.value);
    };

    onTextEditorChange = (value) => {
        const { setEventGeneralDetails } = this.props;
        setEventGeneralDetails('description', value);
    };

    uploadImage = async (fieldName, file, metadata, load, error, progress, abort) => {
        const { setEventGeneralDetails } = this.props;
        const path = Date.now();
        try {
            const response = await Storage.put(path, file, { contentType: file.type });
            load(response);
            setEventGeneralDetails('background_img', path);
        } catch (err) {
            error('Something went wrong! Please try again.');
        }
    };

    fetchBgImg = (url, load, error, progress, abort, headers) => {
        console.log('url', url);
    }

    handleInit() {
        console.log("FilePond instance has initialised", this.pond);
    };

    onPlaceSelected = (suggestion) => {
        const { setEventGeneralDetails } = this.props;
        setEventGeneralDetails("location", suggestion.description);
    }



    onDrop = (pictureFiles, pictureDataURLs) => {
        console.log("pictureFiles", pictureFiles, pictureDataURLs);
        let file = pictureFiles[0];
        const { details, updateGeneralDetail } = this.props;
        Storage.put(file.name, file, {
            contentType: 'image/png'
        }).then(result => {
            console.log("success", result);
            /*this.setState({
                inputImageKey: result.key
            });*/
            details.background_img = result.key;
            updateGeneralDetail(details);

        }).catch(err => {
            console.log("error", err)
        });


    };

    render() {
        const {
            classes,
            event
        } = this.props;

        return (
            <div className={classes.root}>
                <Grid container>
                    <Grid item xs={12} className={classes.control}>
                        <Grid container spacing={24}>
                            <Grid item xs={12}>
                                <FormControl component="fieldset">
                                    <TextField
                                        required
                                        error = {this.state.titleError}
                                        id="signupTitle"
                                        helperText= {this.state.titleText} 
                                        label="Enter title"
                                        value={event.title}
                                        name="title"
                                        onChange={this.handleChange}
                                        margin="normal"
                                        className={classes.textField}
                                        variant="outlined" />
                                </FormControl>
                            </Grid>
                            <Grid item xs={12}>
                                <FormControl component="fieldset">
                                    <FormLabel>Description</FormLabel>
                                    <RichTextEditor
                                        rows={5}
                                        value={event.description}
                                        className="react-rte-demo"
                                        onChange={this.onTextEditorChange}
                                        toolbarClassName="demo-toolbar"
                                        editorClassName="demo-editor" />
                                </FormControl>
                            </Grid>
                            <Grid item xs={12}>
                                {/*<MUIPlacesAutocomplete
                                    name="location"
                                    className={classes.textField}
                                    onSuggestionSelected={this.onPlaceSelected}
                                    renderTarget={() => (<div />)}
                                />*/}
                                <TextField
                                    required
                                    id="searchLocation"
                                    label="Search Location"
                                    margin="normal"
                                    value={event.location}
                                    onChange={this.handleChange}
                                    name="location"
                                    className={classes.textField}
                                variant="outlined"/>
                            </Grid>
                            <Grid item xs={12}>
                                <div className={classes.imageUpload}>
                                    <FilePond
                                        ref={ref => (this.pond = ref)}
                                        oninit={() => this.handleInit()}
                                        allowMultiple={true}
                                        allowFileTypeValidation={true}
                                        acceptedFileTypes={['image/*']}
                                        labelFileTypeNotAllowed='Invalid File'
                                        files={this.state.files}
                                        // onupdatefiles={fileItems => {
                                        //     debugger;
                                        //     // Set currently active file objects to this.state
                                        //     this.setState({
                                        //         files: fileItems.map(fileItem => fileItem.file)
                                        //     });
                                        // }}
                                        server={{
                                            process: this.uploadImage,
                                            fetch: this.fetchBgImg
                                        }} />
                                </div>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

GeneralDetails.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(GeneralDetails);