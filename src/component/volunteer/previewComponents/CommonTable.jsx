import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import amber from '@material-ui/core/colors/amber';


const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    warning: {
        backgroundColor: amber[700],
    },
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
});

class CommonTable extends React.Component {
    state = {
        open: false,
        message: ''
    };

    render() {
        const { classes, event } = this.props;
        return (
            <Paper className={classes.root}>

                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            {/*<TableCell>#</TableCell>*/}
                            <TableCell>Date</TableCell>
                            <TableCell align="center">Time</TableCell>
                            <TableCell align="center">Available Slots</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(Object.keys(event.dateTime)).map((row, index) => (
                            <TableRow key={index}>
                                <TableCell>{event.dateTime[row].startDate}</TableCell>
                                <TableCell>
                                    {event.dateTime[row].timeSlots.map((timeSlot, index) => (
                                        <TableRow rowSpan={event.slots.length} align="center">
                                        <TableCell align="center">
                                        {timeSlot.startTime}-{timeSlot.endTime}
                                        </TableCell>
                                        </TableRow>
                                    ))}
                                </TableCell>
                                <TableCell>
                                    {event.dateTime[row].timeSlots.map((row, index) => (
                                        <div>
                                            {event.slots.map((row, index) => (
                                                <TableRow key={index}>
                                                    <TableCell align="center">{row.title}</TableCell>
                                                    <TableCell align="center">
                                                        <Button variant="contained" color="primary" className={classes.button}>
                                                            Signup
                                                                    </Button>
                                                    </TableCell>
                                                </TableRow>
                                            ))}
                                        </div>
                                    ))}
                                </TableCell>
                            </TableRow>
                        ))}



                    </TableBody>
                </Table>
            </Paper>
        )
    }
}

CommonTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CommonTable);