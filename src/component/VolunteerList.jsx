import React from 'react';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import * as routes from '../routes/path';

const styles = theme => ({
  root: {
    width: '100%',
  },
});

class VolunteerList extends React.Component {
  state = {
  };

  onClickAddEvemt = event => {
    this.props.history.push(routes.VOLUNTEER_ADD)
}

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Grid container alignItems="center">
          <Grid item xs={12}>
            <Grid container>
              <Grid item xs={10}>
                <Typography gutterBottom variant="h4">
                  EVENTS LIST
            </Typography>
              </Grid>
              <Grid item xs={2}>
                <Button variant="contained" color="primary" className={classes.button} onClick={this.onClickAddEvemt}>
                  Add Event
                 </Button>
              </Grid>
          </Grid>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>#</TableCell>
                <TableCell align="center">Events</TableCell>
                <TableCell align="center">Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell>1</TableCell>
                <TableCell align="center">Event 1</TableCell>
                <TableCell align="center">Action</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>2</TableCell>
                <TableCell align="center">Event 2</TableCell>
                <TableCell align="center">Action</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>3</TableCell>
                <TableCell align="center">Event 3</TableCell>
                <TableCell align="center">Action</TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </Grid>
        </Grid>
      </div >
    );
  }
}

VolunteerList.propTypes = {
  classes: PropTypes.object,
};

export default withRouter(withStyles(styles)(VolunteerList));