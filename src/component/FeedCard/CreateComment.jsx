import React, { Component } from 'react';
import {
    Avatar, Input, Grid, Typography
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey';

const styles = theme => ({
    commentTextBox: {
        backgroundColor: grey[50],
        borderRadius: '50px',
        padding: '5px',
        borderStyle: 'solid',
        borderColor: '#bdc3c7',
        borderWidth: '1px'
    },
    helpMessage: {
        color: grey[500]
    }
});

class CreateComment extends Component {

    state = {
        commentText: ''
    }

    componentDidMount() {
        const { comment } = this.props;
        if (comment) {
            this.setState({
                commentText: comment.comment
            })
        }
    }


    handleTextChange = (event) => {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    }

    handleCommentTextKeyPress = (event) => {
        const { commentText } = this.state;
        const { comment } = this.props;

        if (event.key === 'Enter' && commentText && comment) {
            const query = {
                comments: commentText,
                commentid: comment.id,
                postid: comment.postid
            }

            this.props.sendComment(query);
            this.setState({ commentText: '' });
        }

        if (event.key === 'Enter' && !comment) {
            this.props.sendComment(commentText);
            this.setState({ commentText: '' });
        }
    }

    render() {
        const { commentText } = this.state;
        const { currentUser, classes } = this.props;
        const picture = currentUser.userInfo.picture;
        const pictureName = currentUser.userInfo.given_name[0];

        return (
            <Grid container direction="row" spacing={8} style={{ marginBottom: '5px', marginLeft: '5px' }} >
                <Grid item xs={1}>
                    <Avatar
                        alt='user0image'
                        src={picture}
                    >
                        {pictureName}
                    </Avatar>
                </Grid>
                <Grid item container direction="row" xs={10}>
                    <Grid item xs={12} className={classes.commentTextBox} >
                        <Input
                            style={{ width: '100%' }}
                            name="commentText"
                            value={commentText}
                            placeholder="Add a comment.."
                            autoComplete="off"
                            margin="none"
                            disableUnderline
                            onChange={this.handleTextChange}
                            onKeyPress={this.handleCommentTextKeyPress}
                        />
                    </Grid>
                    <Typography variant="caption" gutterBottom className={classes.helpMessage}>
                        Press Enter to post.
                    </Typography>
                </Grid>
            </Grid>
        )
    }
}

export default withStyles(styles)(CreateComment);
