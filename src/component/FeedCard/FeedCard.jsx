import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames'
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import ImagePreview from './ImagePreview';
import CommentBox from './CommentBox';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import Favorite from '@material-ui/icons/Favorite';
import Checkbox from "@material-ui/core/Checkbox";
import Grid from '@material-ui/core/Grid';
import LinesEllipsis from 'react-lines-ellipsis'
import VideoPlayer from '../VideoPlayer';
import grey from '@material-ui/core/colors/grey';
import Divider from '@material-ui/core/Divider';
import msgIcon from '../../assets/images/chat.png';

const propTypes = {
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  card: {
    maxWidth: 650,
    marginBottom: theme.spacing.unit * 2,
    borderRadius: '0px',
  },
  cardShadow: {
    boxShadow: '3px 7px 5px 0px rgba(196,190,190,0.75)',
  },
  cardTitle: {
    color: 'rgba(0, 0, 0, 0.54)',
    fontSize: '13px'
  },
  avatar: {
    backgroundColor: red[500],
  },
  commentBtn: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: 'auto',
    cursor: 'pointer'
  },
  actions: {
    display: 'block',
    padding: '0px'
  },
  p12: {
    padding: '12px'
  },
  likeArea: {
    display: 'flex',
    width: '100%',
    justifyContent: 'space-between'
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  fakeAvatar: {
    backgroundColor: grey[500],
  },
  fake: {
    backgroundColor: grey[200],
    height: theme.spacing.unit,
    margin: theme.spacing.unit * 2,
    // Selects every two elements among any group of siblings.
    '&:nth-child(2n)': {
      marginRight: theme.spacing.unit * 3,
    },
  },
  commentImage: {
    height: '20px',
    marginRight: '2px',
    marginBottom: '2px'
  },
  cardContent: {
    padding: theme.spacing.unit
  },
  padding0: {
    padding: 0,
  }
});

class NewFeed extends React.Component {
  state = {
    expanded: false,
    anchorElPostMenu: null,
    useEllipsis: true,
    favorite: false
  };


  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };

  handlePostMenu = event => {
    this.setState({ anchorElPostMenu: event.currentTarget });
  };

  handlePostClose = () => {
    this.setState({ anchorElPostMenu: null });
  };

  handleDeletePost = () => {
    const { data } = this.props;
    this.props.deletePostAction.request(data);
    this.setState({ anchorElPostMenu: null });
  };

  onTextClick = (e) => {
    e.preventDefault();
    this.setState({ useEllipsis: false })
  }

  handleFavoriteChange = (e) => {
    const { data } = this.props;
    this.setState(prevState => ({ favorite: !prevState.favorite }));
    if (e.target.checked) {
      this.props.likePostAction.request(data);
    } else {
      this.props.unlikePostAction.request(data);
    }
  }

  render() {
    const { anchorElPostMenu, useEllipsis } = this.state;
    const {
      classes, data, currentUser, editMode,
      saveCommentAction, updateCommentAction, deleteCommentAction
    } = this.props;
    const {
      posttext, images, timeago, view_count, likes,
      groupname, user, videos, comments, postid
    } = data;

    const openPostMenu = Boolean(anchorElPostMenu);
    const text = posttext;
    const Images = images.map(image => image.normal);
    const isPostOwner = editMode && user.username === currentUser.userInfo.Username;


    return (
      <Fragment>
        <Card
          className={classnames(
            classes.card,
            { [classes.cardShadow]: editMode }
          )}>
          <CardHeader
            avatar={
              <Avatar aria-label="Recipe" className={classes.avatar} src={user.profile_photo}>
                {user.display_name[0]}
              </Avatar>
            }
            action={
              <Fragment>
                {isPostOwner &&
                  (<IconButton
                    aria-owns={openPostMenu ? 'menu-appbar' : undefined}
                    aria-haspopup="true"
                    onClick={this.handlePostMenu}
                    color="inherit">
                    <MoreHorizIcon />
                  </IconButton>)
                }
                <Popper
                  open={openPostMenu}
                  onClose={this.handlePostClose}
                  anchorEl={anchorElPostMenu}
                  transition
                  disablePortal>
                  {({ TransitionProps, placement }) => (
                    <Grow
                      {...TransitionProps}
                      id="menu-list-grow"
                      style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                    >
                      <Paper>
                        <ClickAwayListener onClickAway={this.handlePostClose}>
                          <Fragment>
                            <MenuList className={classes.padding0}>
                              <MenuItem onClick={this.handlePostClose}>Edit</MenuItem>
                            </MenuList>
                            <MenuList className={classes.padding0}>
                              <MenuItem onClick={this.handleDeletePost}>Delete</MenuItem>
                            </MenuList>
                          </Fragment>
                        </ClickAwayListener>
                      </Paper>
                    </Grow>
                  )}
                </Popper>
                <br />
                <span className={classes.cardTitle}>{view_count} Views</span>
              </Fragment>
            }
            title={
              <b>
                {user.display_name}
                <span className={classes.cardTitle}> posted to  </span>
                <span style={{ color: '#6c757d' }}> {groupname} </span>
              </b>
            }
            subheader={
              <span className={classes.cardTitle}>{timeago}</span>
            }
          />
          <CardContent className={classes.cardContent}>
            <Typography component="div" >
              {useEllipsis
                ? (
                  <Fragment>
                    <LinesEllipsis
                      text={text}
                      maxLine='3'
                      ellipsis={<button className="ag-cell-focus btn-link" onClick={this.onTextClick}> ... more</button>}
                      trimRight
                      basedOn='letters'
                    />
                  </Fragment>
                ) : text}

            </Typography>
            {!!Images.length && editMode && (<ImagePreview Images={Images} data={data} />)}
            <br />
            {!!videos.length && editMode && videos.map(video =>
              (<VideoPlayer
                key={video}
                posterUrl=""
                videoUrl={video}
              />))}
          </CardContent>

          {editMode &&
            (<CardActions className={classes.actions} disableActionSpacing>
              <div className={classes.likeArea}>
                <Grid className={classes.p12}>
                  {likes.count} likes
            </Grid>
                <Typography to="#" className={classes.p12}>
                  {comments.length} comments
              </Typography>
              </div>
              <Divider />
              <div className={classes.likeArea}>
                <Grid>
                  <Checkbox
                    icon={<Favorite />}
                    checkedIcon={<Favorite />}
                    onChange={this.handleFavoriteChange}
                    checked={likes.selflike} />
                  Like
                </Grid>

                <Grid className={classes.p12} onClick={this.handleExpandClick}>
                  <img className={classes.commentImage} src={msgIcon} alt="comment" />
                  Comment
                </Grid>
              </div>
            </CardActions>)
          }

          {editMode && !this.state.expanded && comments.length > 0 ? (
            <CommentBox
              comments={[comments[0]]}
              postid={postid}
              currentUser={currentUser}
              saveCommentAction={saveCommentAction}
              updateCommentAction={updateCommentAction}
              deleteCommentAction={deleteCommentAction}
            />
          ) :

            <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
              <CommentBox
                comments={comments}
                postid={postid}
                currentUser={currentUser}
                saveCommentAction={saveCommentAction}
                updateCommentAction={updateCommentAction}
                deleteCommentAction={deleteCommentAction}
              />
            </Collapse>}
        </Card >
      </Fragment>

    );
  }
}

NewFeed.propTypes = propTypes;

export default withStyles(styles)(NewFeed);

export const FakeCard = withStyles(styles)((props) => {
  const { classes } = props;
  return (
    <Fragment>
      <Card className={classes.card}>
        <CardHeader
          avatar={
            <Avatar aria-label="Recipe" className={classes.fakeAvatar}>
              <div className={classes.fake} />
            </Avatar>
          }
          action={<div className={classes.fake} />}
          title={<div className={classes.fake} />}
          subheader={<div className={classes.fake} />}
        />
        <CardContent>
          <Typography component="div">
            <div className={classes.fake} />
            <div className={classes.fake} />
            <div className={classes.fake} />
          </Typography>
        </CardContent>
      </Card >
    </Fragment>
  )
});