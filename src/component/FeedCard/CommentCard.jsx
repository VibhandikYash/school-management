import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
    Avatar,
    Typography, Grid
} from '@material-ui/core';
// import classNames from 'classnames';
import CreateComment from './CreateComment';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import grey from '@material-ui/core/colors/grey';

const styles = theme => ({
    commentTextGrid: {
        backgroundColor: grey[100],
        borderRadius: '5px',
        padding: '5px',
        width: '90%'
    },
    padding0: {
        padding: 0,
    }

});

class CommentCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            anchorEl: null,
            editMode: false
        }
    }

    handleOpen = (event) => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    handleEdit = (e) => {
        this.setState({
            anchorEl: null,
            editMode: true
        });
    }

    handleDelete = (e) => {
        const { comment } = this.props;
        this.props.deleteCommentAction.request(comment);
        this.setState({ anchorEl: null });
    }

    updateComment = (query) => {
        this.props.updateCommentAction.request(query);
        this.setState({
            editMode: false
        });
    }

    render() {
        const { classes, comment, currentUser } = this.props;
        const { anchorEl, editMode } = this.state;
        const open = Boolean(anchorEl);
        const isCommentOwner = comment.user.username === currentUser.userInfo.Username;

        return (
            <Fragment>
                {!editMode &&
                    <Grid
                        container
                        direction="row"
                        style={{
                            marginBottom: '5px',
                            marginLeft: '5px'
                        }} >
                        <Grid item xs={1}>
                            <Avatar
                                alt='user0image'
                                src={comment.user.profile_photo}
                            >
                                {comment.user.display_name[0]}
                            </Avatar>
                        </Grid>

                        <Grid item xs={10} className={classes.commentTextGrid}>
                            <Typography
                                component="div"
                                paragraph={false}
                            >
                                <span style={{ fontWeight: 'bolder' }}>
                                    {comment.user.display_name} {' '}
                                </span>
                            </Typography>
                            <Typography
                                component="div"
                                paragraph={false}
                            >
                                {comment.comment}
                            </Typography>
                        </Grid>

                        <Grid item xs={1}>
                            {isCommentOwner &&
                                <MoreHorizIcon
                                    aria-owns={open ? 'menu-appbar' : undefined}
                                    aria-haspopup="menu"
                                    onClick={this.handleOpen}
                                />
                            }
                            <Popper
                                open={open}
                                onClose={this.handleClose}
                                anchorEl={anchorEl}
                                transition
                                placement="left-end"
                                disablePortal
                            >
                                {({ TransitionProps, placement }) => (
                                    <Grow
                                        {...TransitionProps}
                                        id="menu-list-grow"
                                        style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                                    >
                                        <Paper>
                                            <ClickAwayListener onClickAway={this.handleClose}>
                                                <Fragment>
                                                    <MenuList className={classes.padding0}>
                                                        <MenuItem onClick={this.handleEdit}>Edit</MenuItem>
                                                    </MenuList>
                                                    <MenuList className={classes.padding0}>
                                                        <MenuItem onClick={this.handleDelete}>Delete</MenuItem>
                                                    </MenuList>
                                                </Fragment>
                                            </ClickAwayListener>
                                        </Paper>
                                    </Grow>
                                )}
                            </Popper>
                        </Grid>
                    </Grid>
                }

                {editMode &&
                    <CreateComment
                        comment={comment}
                        currentUser={currentUser}
                        sendComment={this.updateComment}
                    />
                }

            </Fragment>
        )
    }
}

export default withStyles(styles)(CommentCard);