import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import GeneralDetails from './volunteer/GeneralDetailCardComponent';
import DateTime from './volunteer/DateTimeCardComponent';
import SignupSlot from './volunteer/SignupSlotsCardComponent';
import Preview from './volunteer/PreviewComponent';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing.unit,
    width: '5%'
  },
  nextButton: {
    width: '5%'
  },
  instructions: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
  section3: {
    display: 'flex',
    justifyContent: 'space-between',
  },

});

function getSteps() {
  return ['General Details', 'Dates/Times', 'Slots/Reminders', 'Preview'];
}

function getStepContent(stepIndex) {
  switch (stepIndex) {
    case 0:
      return <GeneralDetails />;
    case 1:
      return <DateTime />;
    case 2:
      return <SignupSlot />;
    case 3:
      return <Preview />;
    default:
      return <div />
  }
}

class HorizontalStepper extends React.Component {
  state = {
    activeStep: 0,
  };

  handleNext = () => {
    this.setState(state => ({
      activeStep: state.activeStep + 1,
    }));
  };

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }));
  };

  changeStep = (activeStep) => {
    const { activeStep: currentStep } = this.state;
    if (activeStep < currentStep)
      this.setState({ activeStep });
  }

  handleReset = () => {
    this.setState({
      activeStep: 0,
    });
  };

  render() {
    const { classes } = this.props;
    const steps = getSteps();
    const { activeStep } = this.state;

    return (
      <div className={classes.root}>
        <div className={classes.section1}>
          <Grid container alignItems="center">
            <Grid item xs={12}>
              <Typography gutterBottom variant="h4">
                <Stepper activeStep={activeStep} alternativeLabel>
                  {steps.map((label, index) => (
                    <Step onClick={() => this.changeStep(index)} key={label}>
                      <StepLabel>{label}</StepLabel>
                    </Step>
                  ))}
                </Stepper>
              </Typography>
            </Grid>
          </Grid>
        </div>
        <div className={classes.section3}>
          <Button
            disabled={activeStep === 0}
            onClick={this.handleBack}
            className={classes.backButton}>
            Back
          </Button>
          <Button className={classes.nextButton} variant="contained" color="primary" onClick={this.handleNext}>
            {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
          </Button>
        </div>
        <div className={classes.section2}>
          <div>
            <div>
              <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

HorizontalStepper.propTypes = {
  classes: PropTypes.object,
};

export default withStyles(styles)(HorizontalStepper);