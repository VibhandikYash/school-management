import React from 'react'
import { Player, BigPlayButton, ControlBar } from 'video-react';
import "video-react/dist/video-react.css";
import DownloadButton from './DownloadButton';

const VideoPlayer = (props) => {
    return (
        <Player
            poster={props.posterUrl}
            src={props.videoUrl}
        >
            <BigPlayButton position="center" />
            <ControlBar autoHide={false}>
                <DownloadButton order={7} />
            </ControlBar>
        </Player>
    );
};

export default VideoPlayer;
