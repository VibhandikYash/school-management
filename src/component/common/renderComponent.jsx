import React from 'react';
import { Checkbox, RadioGroup,  Select } from '@material-ui/core';
import { Input } from 'antd';

const createRenderer = render => ({
    input, meta: {
        touched, error, warning, active,
    }, label, ...rest
}) =>
    (
        <div>
            <label htmlFor="label">
                <span>{label}</span>
            </label>

            {render(input, label, rest)}
            <div
                className={[
                    error && touched ? 'error' : '',
                    active ? 'active' : '',
                ].join(' ')}
            >
                {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
            </div>
        </div >
    );

export const renderTextField = createRenderer((input, label) =>
    <Input placeholder={label} {...input} />)

export const renderCheckbox = ({ input, label }) => (
    <Checkbox
        label={label}
        checked={input.value ? true : false}
        onChange={input.onChange}
    />
)

export const renderRadioGroup = ({ input, ...rest }) => (
    <RadioGroup
        {...input}
        {...rest}
        value={input.value}
        onChange={(event, value) => input.onChange(value)}
    />
)

export const renderSelectField = ({
    input,
    label,
    meta: { touched, error },
    children,
    ...custom
}) => (
        <Select
            inputProps={{
                name: 'age',
                id: 'demo-controlled-open-select',
            }}
            onChange={(event, index, value) => input.onChange(value)}
            children={children}
            {...input}
            {...custom}
        />
    )