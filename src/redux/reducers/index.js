import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import currentUser from './current-user';
import child from './child';
import parent from './parent';
import staff from './staff';
import user from './user';
import attendance from './attendance';
import cache from './cache';
import feed from './feed-post';
import postManage from './post-manage';
import volunteers from '../reducers/volunteer';

const rootReducer = combineReducers({
    form: formReducer,
    currentUser,
    child,
    parent,
    staff,
    user,
    attendance,
    cache,
    feed,
    postManage,
    volunteers
});

export default rootReducer;