import RichTextEditor from 'react-rte';

import {
    ADD_EVENT_SUCCESS,
    ADD_EVENT_FAILURE,
    ADD_EVENT_REQUEST,
    UPDATE_EVENT_STEP,

    SET_EVENT_GENERAL_DETAIL,
    SET_EVENT_GROUPS,

    SET_EVENT_TYPE,

    SET_EVENT_OCCURRENCE,

    SET_RECURRING_TYPE,
    SET_RECURRING_ADD_DAY,
    SET_RECURRING_REMOVE_DAY,

    SET_NOTIFICATION_TYPE,

    SET_EVENT_DATE,
    SET_EVENT_TIME,
    SET_EVENT_REMOVE_SLOT,
    SET_EVENT_ADD_SLOT,
} from "../actions/volunteer";

const INITIAL_STATE = {
    // "id": "92736b9b-eb6d-4ea0-9619-1acac6be5986",
    // event: {
    //     "title": "Testing Event 2",
    //     "description": "<html></html>", // html content from RTE.
    //     "inviteeType": 1,
    //     "location": "Ahmedabad, Gujarat",
    //     "notify": 1,
    //     "background_img": "https://source.unsplash.com/random",
    //     "eventOccurance": {
    //         "occurance": 1,
    //         "occuraceType": 1,
    //         "occuranceDetails": [{
    //                 "date": "2019-08-20", // ISO8601 Complaint date. yyyy-mm-dd
    //                 "timeSlots": [{
    //                     "endTime": "04:30Z", // ISO8601 Complaint time.
    //                     "startTime": "08:30Z"
    //                 }]
    //             },
    //             {
    //                 "date": "2019-08-21",
    //                 "timeSlots": [{
    //                         "endTime": "11:30Z",
    //                         "startTime": "09:30Z"
    //                     },
    //                     {
    //                         "endTime": "02:30Z",
    //                         "startTime": "12:30Z"
    //                     }
    //                 ]
    //             }
    //         ],
    //     },
    //     "groups": [{
    //         "groupId": 4,
    //         "parents": [
    //             "parent1@email.com",
    //             "parent2@email.com"
    //         ]
    //     }],
    //     "slots": [{
    //             "comment": "Get 1 ltr Bottles",
    //             "requiredCount": 20,
    //             "title": "Coke"
    //         },
    //         {
    //             "comment": "Get 500gms pack",
    //             "requiredCount": 30,
    //             "title": "Bread"
    //         }
    //     ],
    // },
    event: {
        title: '',
        description: RichTextEditor.createEmptyValue(),
        background_img: '',
        groups: [],
        recurringType: '',
        recurringDays: [],
        dateTime: {},
        location:'',
        groupParentSelection:'',
        occuranceType: '',
        notificationType:'',
        eventOccurrence: {
            occurrence: '',
            eventDate: '',
            eventStartTime: '',
            eventEndTime: '',
            eventStartDate: '',
            eventEndDate: '',
        },
        slots: []
    },
    events: [],
    loading: false,
    errors: []
}
    ;

const volunteers = (state = INITIAL_STATE, {
    payload,
    errors,
    ...action
}) => {
    switch (action.type) {

        case UPDATE_EVENT_STEP:
            return {
                ...state,
                event: {
                    ...state.event,
                    ...payload
                }
            }

        case SET_EVENT_GENERAL_DETAIL:
            let param = payload['name'];
            let value = payload['value'];
            return {
                ...state,
                event: {
                    ...state.event,
                    [param]: value
                }
            };

        case SET_EVENT_GROUPS:
            return {
                ...state,
                event: {
                    ...state.event,
                    groups: payload
                }
            };

        case SET_NOTIFICATION_TYPE:
            return {
                ...state,
                event: {
                    ...state.event,
                    notificationType: payload,
                }
            };

        case SET_EVENT_TYPE:
            return {
                ...state,
                event: {
                    ...state.event,
                    occuranceType: payload,
                    dateTime: {}
                }
            };


        case SET_RECURRING_TYPE:
            return {
                ...state,
                event: {
                    ...state.event,
                    recurringType: payload,
                }
            };

        case SET_RECURRING_ADD_DAY:
            const selectedDays = state.event.recurringDays.push(payload);
            return {
                ...state,
                event: {
                    ...state.event,
                    recurringDay: selectedDays,
                }
            };

        case SET_RECURRING_REMOVE_DAY:
            const removedDays = state.event.recurringDays.filter(day => day !== payload)
            return {
                ...state,
                event: {
                    ...state.event,
                    recurringDay: removedDays,
                }
            };

        case SET_EVENT_OCCURRENCE:
            return {
                ...state,
                event: {
                    ...state.event,
                    eventOccurrence: {
                        ...state.event.eventOccurrence,
                        occurrence: payload
                    }
                }
            };

        case SET_EVENT_DATE:
            console.log("in reducer", SET_EVENT_DATE, payload);
            return {
                ...state,
                event: {
                    ...state.event,
                    dateTime: {
                        ...state.event.dateTime,
                        [payload.index]: {
                            ...state.event.dateTime[payload.index],
                            [payload.dateType]: payload.date
                        }
                    }
                }
            };

        case SET_EVENT_TIME:
            // console.log("payload", payload);
            const timeSlots = state.event.dateTime[payload.index].timeSlots;
            if (!timeSlots[payload.timeIndex]) {
                timeSlots[payload.timeIndex] = {};
            }
            timeSlots[payload.timeIndex][payload.timeType] = payload.time;
            return {
                ...state,
                event: {
                    ...state.event,
                    dateTime: {
                        ...state.event.dateTime,
                        [payload.index]: {
                            ...state.event.dateTime[payload.index],
                            timeSlots
                        }
                    }
                }
            };

        case SET_EVENT_ADD_SLOT:
            return {
                ...state,
                event: {
                    ...state.event,
                    dateTime: {
                        ...state.event.dateTime,
                        [payload.index]: {
                            ...state.event.dateTime[payload.index],
                            timeSlots: (state.event.dateTime[payload.index].timeSlots || [])
                                .concat([{ startTime: payload.startTime, endTime: payload.endTime }])
                        }
                    }
                }
            };

        case SET_EVENT_REMOVE_SLOT:
            return {
                ...state,
                event: {
                    ...state.event,
                    dateTime: {
                        ...state.event.dateTime,
                        [payload.index]: {
                            ...state.event.dateTime[payload.index],
                            timeSlots: state.event.dateTime[payload.index].timeSlots.filter((time, index) => index !== payload.timeIndex)
                        }
                    }
                }
            };

        case ADD_EVENT_REQUEST:
            return {
                ...state,
                loading: true
            };

        case ADD_EVENT_SUCCESS:
            return {
                ...state,
                event: payload
            };

        case ADD_EVENT_FAILURE:
            return {
                ...state,
                errors
            };

        default:
            return state
    }
};

export default volunteers;