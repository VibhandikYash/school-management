import {
    HANDLE_TEXT_CHANGE,
    UPLOAD_IMAGE,
    UPLOAD_VIDEO,
    REMOVE_SELECTED_VIDEO,
    REMOVE_SELECTED_IMAGE,
    OPEN_DIALOG,
    CLOSE_DIALOG
} from "../actions/post-manage";
import {
    SUCCESS,
    REQUEST
} from "../actions/common";
import {
    CREATE_POST
} from "../actions/feed-post";

const INITIAL_STATE = {
    selectedDate: null,
    images: [],
    videos: [],
    groupids: '',
    text: '',
    imageLoading: false,
    videoLoading: false,
    showDialog: false,
    postLoading: false,
}

const postManage = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case OPEN_DIALOG:
            {
                return {
                    ...state,
                    showDialog: true
                }
            }
        case CLOSE_DIALOG:
            {
                return {
                    ...state,
                    showDialog: false
                }
            }
        case HANDLE_TEXT_CHANGE:
            return {
                ...state,
                [action.name]: action.value
            }
        case UPLOAD_IMAGE[REQUEST]:
            return {
                ...state,
                imageLoading: true
            }
        case UPLOAD_IMAGE[SUCCESS]:
            return {
                ...state,
                images: action.payload,
                imageLoading: false
            }
        case UPLOAD_VIDEO[REQUEST]:
            return {
                ...state,
                videoLoading: true
            }
        case UPLOAD_VIDEO[SUCCESS]:
            return {
                ...state,
                videos: action.payload,
                videoLoading: false

            }
        case REMOVE_SELECTED_IMAGE:
            return {
                ...state,
                ...state.images.splice(action.index, 1)
            }
        case REMOVE_SELECTED_VIDEO:
            return {
                ...state,
                ...state.videos.splice(action.index, 1)
            }
        case CREATE_POST[SUCCESS]:
            {
                return {
                    ...INITIAL_STATE
                }
            }
        default:
            return state
    }
}

export default postManage;