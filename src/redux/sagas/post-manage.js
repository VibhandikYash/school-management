import {
    Storage
} from 'aws-amplify';
import moment from 'moment';
import {
    v4
} from 'uuid';

import {
    takeEvery,
    fork,
    all,
    put,
} from 'redux-saga/effects';
import {
    UPLOAD_IMAGE,
    uploadImage,
    uploadVideo,
    UPLOAD_VIDEO
} from '../actions/post-manage';
import {
    REQUEST
} from '../actions/common';

async function saveInAWSStorage(files) {
    return await Promise.all(files.map(async (file) => {
        const path = `${moment().year()}/${moment().month() + 1}/${moment().date()}/${v4()}.${file.type.split('/')[1]}`;
        const {
            key
        } = await Storage.put(path, file, {
            contentType: file.type
        });
        return await Object.assign(file, {
            preview: URL.createObjectURL(file),
            key: `public/${key}`
        })
    }));
}


function* uploadImageSW(action) {
    try {
        const images = yield saveInAWSStorage(action.query);
        yield put(uploadImage.success(images))
    } catch (error) {

    }
}

function* uploadVideoSW(action) {
    try {
        const images = yield saveInAWSStorage(action.query);
        yield put(uploadVideo.success(images))
    } catch (error) {

    }
}

/** watcher */
function* watcherImageUpload() {
    yield takeEvery(UPLOAD_IMAGE[REQUEST], uploadImageSW)
}

function* watcherVideoUpload() {
    yield takeEvery(UPLOAD_VIDEO[REQUEST], uploadVideoSW)
}

export default function* rootPostManage() {
    yield all([
        fork(watcherImageUpload),
        fork(watcherVideoUpload),
    ]);
}