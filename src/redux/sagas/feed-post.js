import {
    API
} from 'aws-amplify';
import {
    takeEvery,
    fork,
    all,
    put,
    select
} from 'redux-saga/effects'

import {
    fetchPost,
    FETCH_POST,
    CREATE_POST,
    SAVE_COMMENT,
    saveComment,
    UPDATE_COMMENT,
    updateComment,
    DELETE_COMMENT,
    deleteComment,
    DELETE_POST,
    EDIT_POST,
    deletePost,
    LIKE_POST,
    UNLIKE_POST,
    likePost,
    unlikePost,
    createPost
} from '../actions/feed-post'
import {
    REQUEST
} from '../actions/common';
import { handleCloseDialog } from '../actions/post-manage';

function* fetchFeedPost(action) {
    try {
        const init = {
            body: action.query
        }
        const payload = yield API.post('get_posts', '', init)
        if (!payload.errorMessage) {
            yield put(fetchPost.success(payload))
        } else {
            throw payload.errorMessage
        }
    } catch (error) {
        yield put(fetchPost.failure(error))
    }
}

function* createFeedPost() {
    try {
        const {
            groupids,
            text,
            images,
            videos
        } = yield select(state => state.postManage);
        const Groups = yield select(state => state.currentUser.userInfo.Groups);

        const body = {
            groupids,
            groupnames: groupids ? Groups[groupids].GroupName : '',
            post: text,
            img: images.length ? images.map(image => image.key) : [],
            vid: videos.length ? videos.map(video => video.key) : [],
        }
        const init = {
            body
        }
        yield API.post('add_posts', '', init)
        const query = yield select(state => state.feed.query);
        yield query.page = 0;
        yield put(createPost.success());
        yield put(handleCloseDialog());
        yield put(fetchPost.request(query))
    } catch (error) {

    }
}

function* createEditPost(action) {
    yield console.log(action);
}

function* createDeletePost(action) {
    const init = {
        body: {
            postid: action.query.postid
        }
    }
    yield API.post('delete_posts', '', init)
    yield put(deletePost.success(init.body))
}

function* createSaveComment(action) {
    try {
        const init = {
            body: action.query
        }
        const user = yield select(state => state.currentUser.userInfo.email);
        const posts = yield select(state => state.feed.posts);
        const post = posts.find(post => post.postid === action.query.postid);
        const {
            data
        } = yield API.post('add_comments', '', init);
        const payload = {
            id: data,
            postid: action.query.postid,
            comment: action.query.comment,
            user,
            updatedtime: "a moment ago",
        }
        yield post.comments.unshift(payload);
        yield put(saveComment.success(post))
    } catch (error) {

    }
}

function* createUpdateComment(action) {
    try {
        const init = {
            body: action.query
        }
        const posts = yield select(state => state.feed.posts);
        const post = posts.find(post => post.postid === action.query.postid);
        const updatedComments = yield post.comments.map(comment => {
            if (comment.id === action.query.commentid) {
                comment.comment = action.query.comments;
                comment.updatedtime = "a moment ago";
            }
            return comment
        });
        post.comments = updatedComments;
        yield API.post('edit_comments', '', init);
        yield put(updateComment.success(post));
    } catch (error) {

    }
}

function* createDeleteComment(action) {
    try {
        const init = {
            body: {
                commentid: action.query.id
            }
        }
        const posts = yield select(state => state.feed.posts);
        const post = posts.find(post => post.postid === action.query.postid);
        const updatedComments = yield post.comments.filter(comment => comment.id !== action.query.id);
        post.comments = updatedComments;
        yield API.post('delete_comments', '', init);
        yield put(deleteComment.success(post))
    } catch (error) {

    }
}

function* createLikePost(action) {
    const init = {
        body: {
            postid: action.query.postid
        }
    }
    const user = yield select(state => state.currentUser.userInfo.email);
    yield put(likePost.success({
        ...init.body,
        user
    }))
    yield API.post('post_like', '', init)
}

function* createUnlikePost(action) {
    const init = {
        body: {
            postid: action.query.postid
        }
    }
    const user = yield select(state => state.currentUser.userInfo.email);
    yield put(unlikePost.success({
        ...init.body,
        user
    }))
    yield API.post('post_unlike', '', init)
}

/** watcher  */

function* watcherFetchFeedPost() {
    yield takeEvery(FETCH_POST[REQUEST], fetchFeedPost)
}

function* watcherCreateFeedPost() {
    yield takeEvery(CREATE_POST[REQUEST], createFeedPost)
}

function* watcherEditPost() {
    yield takeEvery(EDIT_POST[REQUEST], createEditPost)
}

function* watcherDeletePost() {
    yield takeEvery(DELETE_POST[REQUEST], createDeletePost)
}

function* watcherSaveComment() {
    yield takeEvery(SAVE_COMMENT[REQUEST], createSaveComment)
}

function* watcherUpdateComment() {
    yield takeEvery(UPDATE_COMMENT[REQUEST], createUpdateComment);
}

function* watcherDeleteComment() {
    yield takeEvery(DELETE_COMMENT[REQUEST], createDeleteComment);
}

function* watcherLikeComment() {
    yield takeEvery(LIKE_POST[REQUEST], createLikePost);

}

function* watcherUnlikeComment() {
    yield takeEvery(UNLIKE_POST[REQUEST], createUnlikePost);

}

export default function* rootCache() {
    yield all([
        fork(watcherCreateFeedPost),
        fork(watcherEditPost),
        fork(watcherDeletePost),
        fork(watcherFetchFeedPost),
        fork(watcherSaveComment),
        fork(watcherUpdateComment),
        fork(watcherDeleteComment),
        fork(watcherLikeComment),
        fork(watcherUnlikeComment),
    ]);
}