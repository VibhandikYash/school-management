import {
    takeEvery,
    fork,
    all,
    put,
    select
} from 'redux-saga/effects'
import VolunteerService from '../../service/volunteer'
import { RecurringOptions, Occurances } from '../../shared/enums';
import moment from 'moment';
import {
    // ADD_EVENT_SUCCESS,
    // ADD_EVENT_FAILURE,
    ADD_EVENT_REQUEST,
    addEventSuccess,
    addEventFailure,
    SET_EVENT_DATE,
    // SET_EVENT_OCCURRENCE,
    SET_EVENT_TYPE,
    SET_RECURRING_ADD_DAY,
    SET_RECURRING_TYPE,
    updateEventStep
} from '../actions/volunteer'

function* createEvent(action) {
    try {
        const event = yield select(state => state.volunteers.event);
        const res = yield VolunteerService.createEvent(event);
        yield put(addEventSuccess(res));
    } catch (error) {
        console.log(error);
        // modularize error object and return array!
        yield put(addEventFailure(error));
    }
}

function getAllEventDates(startDate, endDate, week, recurringType) {
    const momentStartDate = moment(startDate);
    const momentEndDate = moment(endDate);
    const dates = [];
    if (recurringType === RecurringOptions.repeatEveryWeek) {
        for (let m = moment(momentStartDate); m.diff(momentEndDate, 'days') <= 0; m.add(1, 'days')) {
            if (week.includes(m.isoWeekday().toString())) {
                dates.push(m.utc().toDate());
            }
        }
    } else if (recurringType === RecurringOptions.repeatEveryMonthSameNumericDay) {
        for (let m = moment(momentStartDate); m.diff(momentEndDate, 'day') <= 0; m.add(1, 'month')) {
            if (m.isValid()) {
                dates.push(m);
            }
        }
    }
    return dates;
}

function* CalculateDates(action) {
    try {
        console.log('in calculate days');
        const eventData = yield select(state => state.volunteers.event);
        const { dateTime, recurringType, recurringDays, occuranceType } = eventData;
        const date = dateTime[0];
        const { startDate, endDate } = date;
        console.log((occuranceType === Occurances.recurring || occuranceType === Occurances.timeslots) && !!date.startDate && !!date.endDate);
        if (!!(occuranceType === Occurances.recurring || occuranceType === Occurances.timeslots) && !!date.startDate && !!date.endDate) {
            const eventDates = getAllEventDates(startDate, endDate, recurringDays, recurringType);
            yield put(updateEventStep({ eventDates }));
        }
        // console.log('in saga', eventData);
    } catch (err) {
        console.log()
    }
}

function* watchCreateEvent() {
    yield takeEvery(ADD_EVENT_REQUEST, createEvent)
}

function* watchEveryTimeEvent() {
    yield takeEvery([SET_EVENT_DATE, SET_RECURRING_TYPE, SET_EVENT_TYPE, SET_RECURRING_ADD_DAY], CalculateDates)
}

export default function* rootVolunteer() {
    yield all([
        fork(watchCreateEvent),
        fork(watchEveryTimeEvent),
    ]);
}