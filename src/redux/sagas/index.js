import { all,fork } from 'redux-saga/effects';
import rootCache from './cache';
import rootFeedPost from './feed-post';
import rootPostManage from './post-manage';
import rootVolunteer from './volunteer';

export default function* root() {
    yield all([
      fork(rootCache),
      fork(rootFeedPost),
      fork(rootPostManage),
      fork(rootVolunteer),
    ]);
  }