import { all, put, call, takeEvery, select } from 'redux-saga/effects';
import axios from 'axios';
import * as actions from '../actions';
import constants from '../constants';
import endpoints from '../../components/Helper/Config';
import qs from 'qs';
import _ from 'lodash';

function convertFiltersToQS(filters) {
    return qs.stringify(filters);
}

function getDetails(filters) {
    let qs = convertFiltersToQS(filters);
    // if (query)
    //     qs += `&${query}`;
    return axios.get(`${endpoints.baseURL}${endpoints.SeatAllotments}?${qs}`).then(res => {
        const totalRecords = parseInt(res.headers['x-total-count'], 10);
        return {
            seatAllotments: res.data,
            totalRecords
        };
    });
}

const getFilters = state => state.SeatAllotments.filters;

const getFiltersByIds = filters => {
    const filtersById = {};
    for (const key in filters) {
        if (!.isNull(filters[key]) || !.isEmpty(filters[key]))
            filtersById[key] = !_.isUndefined(filters[key].id) ? filters[key].id : filters[key];
    }
    console.log(filtersById)
    return filtersById;
}

function* getSeatAllotments(action) {
    try {
        const existingFilters = yield select(getFilters);
        const filters = { ...existingFilters, ...action.filters };
        const seatAllotments = yield call(getDetails, getFiltersByIds(filters));
        yield put(actions.getSeatAllotmentSuccess(seatAllotments));
    } catch (err) {
        yield put(actions.getSeatAllotmentFailure(err.response.data.errors));
    }
}

function* filterSeatAllotments({ payload: newFilters }) {
    try {
        const existingFilters = yield select(getFilters);
        const filters = { ...existingFilters, ...newFilters };
        yield put(actions.getSeatAllotment(filters));
    } catch (err) {
        console.log(err);
    }
}

//Watcher Saga
export default function* WatchSeatAllotments() {
    yield all([
        takeEvery(constants.SET_SEAT_ALLOTMENT_FILTER, getSeatAllotments),
        takeEvery(constants.SEAT_ALLOTMENT_REQ, filterSeatAllotments)
    ])
}