export const ADD_EVENT_REQUEST = 'ADD_EVENT_REQUEST';
export const ADD_EVENT_SUCCESS = 'ADD_EVENT_SUCCESS';
export const ADD_EVENT_FAILURE = 'ADD_EVENT_FAILURE';
export const UPDATE_EVENT_STEP = 'UPDATE_EVENT_STEP';
export const SET_EVENT_GENERAL_DETAIL = 'SET_EVENT_GENERAL_DETAIL';
export const SET_EVENT_GROUPS = 'SET_EVENT_GROUPS';
export const SET_EVENT_TYPE = 'SET_EVENT_TYPE';
export const SET_EVENT_OCCURRENCE = 'SET_EVENT_OCCURRENCE';
export const SET_EVENT_DATE = 'SET_EVENT_DATE';
export const SET_EVENT_TIME = 'SET_EVENT_TIME';
export const SET_EVENT_ADD_SLOT = 'SET_EVENT_ADD_SLOT';
export const SET_EVENT_REMOVE_SLOT = 'SET_EVENT_REMOVE_SLOT';
export const SET_RECURRING_TYPE = 'SET_RECURRING_TYPE';
export const SET_RECURRING_ADD_DAY = 'SET_RECURRING_ADD_DAY';
export const SET_RECURRING_REMOVE_DAY = 'SET_RECURRING_REMOVE_DAY';
export const SET_NOTIFICATION_TYPE = 'SET_NOTIFICATION_TYPE';


export const updateEventStep = (stepData = {}) => {
    return {
        type: UPDATE_EVENT_STEP,
        payload: stepData
    }
};

export const setEventGeneralDetails = (data) => {
    return {
        type: SET_EVENT_GENERAL_DETAIL,
        payload: data
    }
};

export const setEventGroups = (groups) => {
    return {
        type: SET_EVENT_GROUPS,
        payload: groups
    }
};

export const setEventType = (occuranceType) => {
    return {
        type: SET_EVENT_TYPE,
        payload: occuranceType
    }
};

export const setOccuranceType = (recurringType) => {
    return {
        type: SET_RECURRING_TYPE,
        payload: recurringType
    }
};

export const setRecurringAddDay = (recurringDay) => {
    return {
        type: SET_RECURRING_ADD_DAY,
        payload: recurringDay
    }
};

export const setRecurringRemoveDay = (recurringDay) => {
    return {
        type: SET_RECURRING_REMOVE_DAY,
        payload: recurringDay
    }
};

export const setEventOccurrence = (occurrence) => {
    return {
        type: SET_EVENT_OCCURRENCE,
        payload: occurrence
    }
};

export const setNotificationType = (notificationType) => {
    return {
        type: SET_NOTIFICATION_TYPE,
        payload: notificationType
    }
};

export const setEventDate = (payload) => {
    return {
        type: SET_EVENT_DATE,
        payload
    }
};

export const setEventTime = (payload) => {
    return {
        type: SET_EVENT_TIME,
        payload
    }
};

export const setEventAddSlot = (payload) => {
    return {
        type: SET_EVENT_ADD_SLOT,
        payload
    }
};

export const setEventRemoveSlot = (payload) => {
    return {
        type: SET_EVENT_REMOVE_SLOT,
        payload
    }
};

export const setEventEndTime = (payload) => {
    return {
        type: SET_EVENT_TIME,
        payload
    }
};

export const setEventStartTime = (payload) => {
    return {
        type: SET_EVENT_TIME,
        payload
    }
};

export const addEventReq = event => ({
    type: ADD_EVENT_SUCCESS,
    event
});

export const addEventSuccess = event => ({
    type: ADD_EVENT_SUCCESS,
    payload: event
});

export const addEventFailure = errors => {
    return {
        type: ADD_EVENT_FAILURE,
        errors
    }
};