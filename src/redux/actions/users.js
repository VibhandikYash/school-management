export const FETCH_BULK_USERS_LIST_SUCCESS = 'FETCH_BULK_USERS_LIST_SUCCESS';

export const fetchBulkUsersList = data => ({
    type: FETCH_BULK_USERS_LIST_SUCCESS,
    data
})

 
