import {
    action, createRequestTypes, REQUEST, SUCCESS, FAILURE
} from './common';

export const OPEN_DIALOG = 'OPEN_DIALOG';
export const handleOpenDialog = (query)=>action(OPEN_DIALOG,{query});

export const CLOSE_DIALOG = 'CLOSE_DIALOG';
export const handleCloseDialog = (query)=>action(CLOSE_DIALOG,{query});

export const HANDLE_TEXT_CHANGE = 'HANDLE_TEXT_CHANGE';
export const handleTextChange = (name, value) => action(HANDLE_TEXT_CHANGE, {
    name,
    value
});

export const UPLOAD_IMAGE = createRequestTypes('UPLOAD_IMAGE');
export const uploadImage ={
    request: query => action(UPLOAD_IMAGE[REQUEST], {query}),
    success: payload => action(UPLOAD_IMAGE[SUCCESS], {payload}),
    failure: error => action(UPLOAD_IMAGE[FAILURE], {error}),
}

export const REMOVE_SELECTED_IMAGE = 'REMOVE_SELECTED_IMAGE';
export const removeSelectedImage = index=>action(REMOVE_SELECTED_IMAGE,{index});

export const REMOVE_SELECTED_VIDEO = 'REMOVE_SELECTED_VIDEO';
export const removeSelectedVideo = index=>action(REMOVE_SELECTED_VIDEO,{index});

export const UPLOAD_VIDEO = createRequestTypes('UPLOAD_VIDEO');
export const uploadVideo ={
    request: query => action(UPLOAD_VIDEO[REQUEST], {query}),
    success: payload => action(UPLOAD_VIDEO[SUCCESS], {payload}),
    failure: error => action(UPLOAD_VIDEO[FAILURE], {error}),

}