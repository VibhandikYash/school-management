import {
    Auth
} from 'aws-amplify';

// AWS Mobile Hub Project Constants
const setHeader = async () => {
    const token = (await Auth.currentSession()).idToken.jwtToken;
    return {
        'Authorization': token,
        'content-type': 'application/json'
    }
};

const API = {
    'graphql_endpoint': 'https://3tfntf2atnfszabdcv3wsjjoa4.appsync-api.us-east-1.amazonaws.com/graphql',
    'graphql_headers': async () => {
        const token = (await Auth.currentSession()).idToken.jwtToken;
        return ({
            'Authorization': token
        })
    },
    // 'graphql_endpoint_iam_region': 'my_graphql_apigateway_region',
    'endpoints': [{
        'name': "reset_password",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/users/resetpasswordflow",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "get_user_info",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/me",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "get_students",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/child",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "add_students",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/child/add",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "edit_students",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/child/update",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "delete_student",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/child/delete",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "search_user",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/users/search",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "bulupload",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/bulkupload",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "bulupload_status",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/bulkupload/status",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "get_users",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/users",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "add_users",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/users/add",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "add_users_db",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/users/adddb",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "edit_users",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/users/update",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "delete_users",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/users/delete",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "delete_users_db",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/users/deletedb",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "get_groups",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/groups",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "add_group",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/groups/add",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "delete_group",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/groups/delete",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "update_group",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/groups/update",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "get_users_count",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/dashboard/usercount",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "get_students_count",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/dashboard/studentcount",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "get_lastlogin",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/users/lastlogin",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "get_user_detail",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/users/details",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "get_user_child_spouse",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/users/students",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "add_groups_students_add",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/groups/students/add",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "child_group_delete",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/child/group-delete",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "get_child_by_id",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/child/id",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "unmarked_attendance",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/attendance/today/unmarked",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "summary_attendance",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/attendance/today/summary",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "summarybyclass",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/attendance/summarybyclass",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "child_attendance",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/child/attendance",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "mark_attendance",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/admin/attendance/markattendance",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "user_cache",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/config/usercache",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "get_posts",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/posts",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "add_posts",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/posts/add",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "edit_posts",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/posts/edit",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "delete_posts",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/posts/delete",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "add_comments",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/posts/comments/add",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "edit_comments",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/posts/comments/edit",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "delete_comments",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/posts/comments/delete",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "post_like",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/posts/like",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "post_unlike",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/posts/unlike",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    {
        'name': "post_view",
        'endpoint': "https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/posts/view",
        'service': "lambda",
        'region': "us-east-1",
        'custom_header': setHeader
    },
    ]
};

export default API;