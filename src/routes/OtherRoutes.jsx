import React, { Fragment } from 'react';
import * as routes from './path';
import { Route } from 'react-router-dom';
import Welcome from '../container/Home';
import ComingSoon from '../container/ComingSoon';
import Volunteer from '../container/Volunteer';
import AddEditVolunteer from '../container/AddEditVolunteer';

export default function OtherRoutes() {
    return (
        <Fragment>
            <Route component={Welcome} path={routes.WELCOME} exact />
            <Route component={ComingSoon} path={routes.NEWSLETTER} exact />
            <Route component={Volunteer} path={routes.VOLUNTEER} exact />
            <Route component={ComingSoon} path={routes.PORTFOLIO} exact />
            <Route component={AddEditVolunteer} path={routes.VOLUNTEER_ADD} exact />
        </Fragment>
    );
}