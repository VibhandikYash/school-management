export const Occurances = Object.freeze({
    "onetime": 1,
    "recurring": 2,
    "timeslots": 3,
    "multipletimeslots": 4
});