export const RecurringOptions = Object.freeze({
    "repeatEveryWeek": 1,
    "repeatEveryMonthSameNumericDay": 2,
    "repeatEveryMonthSameRelativeDay": 3
});